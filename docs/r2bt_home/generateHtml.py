#!/bin/python

import jinja2

TEMPLATE_FILE = "r2bt.xhtml"
OUTPUT_FILE = "index.xhtml"

with open(TEMPLATE_FILE) as f:
    xhtml = f.read().replace("<title>R2BT</title>", "<title>R2BT</title>\n{{IncludeCSS}}")
    template = jinja2.Template(xhtml)

tags = {
    'Hola': 'Digo hola',
    'Scripts': '''\
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="http://threejs.org/build/three.min.js"></script>	
	<script src="r2bt.js"></script>
    ''',
    'Canvas': '<canvas width="600px" height="600px" id="the_canvas"></canvas>',
    'IncludeCSS': '<link rel="stylesheet" type="text/css" href="r2bt.css"/>'
}

# outputText = template.render(tags)

with open(OUTPUT_FILE, "w") as txt:
    txt.write(template.render(tags))