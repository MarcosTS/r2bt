/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_SS_INSERT_23503U45TWEJGFR5
#define _R2BT_SS_INSERT_23503U45TWEJGFR5

namespace r2bt {

template<typename Iterator, typename Neighborhood, typename Functor>
void insert(Neighborhood* neigh, Iterator first, Iterator last, Functor point2coordinates)
{
    insert(*neigh, first, last, point2coordinates);
}

template<typename Iterator, typename Neighborhood, typename Functor>
void insert(Neighborhood& neigh, Iterator first, Iterator last, Functor point2coordinates)
{
    typedef typename Neighborhood::PointAdapter Adapter;
    typedef typename std::iterator_traits<Iterator>::value_type Point;
    typedef typename std::remove_pointer<Point>::type PointWitoutPointer;
    typedef typename std::iterator_traits<Iterator>::iterator_category iterator_tag;
    Iterator ite;

    // First count points ... Note that the count is based on the tessel
    LOG_OBJ("Counting cells to create...\n");
    for ( ite = first; ite != last; ++ite )
    {
        Adapter a;
        set_coordinates_and_tessel<Adapter,3>( a, neigh.get_range(), point2coordinates(*ite) );
        neigh.count_point( a.get_tessel() );
    }
    LOG_OBJ("Counting cell finished\n");

    // Use previous count to dynamically create the cells
    LOG_OBJ("Creating cells...\n");
    neigh.create_cells();
    LOG_OBJ("Creating cells finished\n");

    // Now insert the points
    for ( ite = first; ite != last; ++ite )
    {
        Adapter a;
        set_coordinates_and_tessel<Adapter,3>( a, neigh.get_range(), point2coordinates(*ite) );
        set_address<Adapter, PointWitoutPointer>( a, *ite );
        compute_diff<Adapter, Iterator>( a, first, ite, iterator_tag{} );
        neigh.insert( a );
    }

    // Now complete setup for the neighborhood
    neigh.build();
}

} //namespace

#endif // _R2BT_SS_INSERT_23503U45TWEJGFR5
