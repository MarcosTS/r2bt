/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_POINT_ADAPTER_A02RUAFJIOQ34EI4OQ02
#define _R2BT_POINT_ADAPTER_A02RUAFJIOQ34EI4OQ02

namespace r2bt {

template <unsigned DIM>
class point_adapter
{
public:
    inline void update_reference(uintptr_t ref_)
    {
        ref = ref_;
    }

    inline void update_coordinates(const std::array<double, DIM>& coord_)
    {
        coord = coord_;
    }

    inline void update_coordinates_tessel(const std::array<double, DIM>& coord_, int range)
    {
        coord = coord_;
        point_tessel = tessel<DIM>::get_tessel(coord, range);
    }

    inline const std::array<double, DIM>& get_coordinates() const
    {
        return coord;
    }

    inline uintptr_t get_address() const
    {
        return ref;
    }

    inline const tessel<DIM>& get_tessel() const
    {
        return point_tessel;
    }

private:
    uintptr_t ref;
    std::array<double, DIM> coord;
    tessel<DIM> point_tessel;
};

} //namespace

#endif // _R2BT_POINT_ADAPTER_A02RUAFJIOQ34EI4OQ02
