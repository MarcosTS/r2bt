/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fern ndez (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_BIT_LATTICE_922FN0IWBFIWIF2FI2I2F
#define _R2BT_BIT_LATTICE_922FN0IWBFIWIF2FI2I2F

namespace r2bt {

/*  32 BIT

    Negative overflow:      -(2-2e-23) x 2e127
    Negative underflow:     -2e-149                 Normalized: -2e-126
    Zero
    Positive underflow:      2e-149                 Normalized:  2e-126
    Positive overflow:      (2-2e-23) x 2e127

    64 BIT

    Negative overflow:      -(2-2e-52) x 2e1023
    Negative underflow:     -2e-1074                Normalized: -2e-1022
    Zero
    Positive underflow:      2e-1074                Normalized:  2e-1022
    Positive overflow:      (2-2e-52) x 2e1023

    El IEEE reserva para exponentes compuestos por todo 0s y todo 1s,
    unos valores especiales: infinitos, desnormalizados, NaN, etc */

inline int64_t bit_lattice(double x, int range)
{
    if ( x > 0 && x < exp2(range) )
    {
        return 0;
    }
    else if ( x < 0 && -x < exp2(range) )
    {
        return -1;
    }

    const uint64_t SIGN_BIT = 0x8000000000000000Lu;
    const uint64_t EXPO_BIT = 0x7FF0000000000000Lu;
    const uint64_t CAND_BIT = 0x000FFFFFFFFFFFFFLu;
    //const u_int64_t MANT_BIT = 0x0010000000000000Lu;  // Implict 1 of the mantissa

    union number {
        double re;
        long long unsigned i;
    };
    union number u;
    u.re = x;

    int sign = u.i & SIGN_BIT ? 1 : 0;
    uint64_t exponent = (u.i & EXPO_BIT) >> 52ULL;
    uint64_t significand = (u.i & CAND_BIT) + (1ULL << 52ULL);

    if ( !exponent ) // If exponent is all zeros, it is denormalized
    {
        return sign ? -1 : 0;
    }

    /* significand is *2^52 and exponent exceed is 1023 */
    uint64_t u_result = significand >> ( 1023 - exponent + range + 52 );
    if ( sign ) /* negative */
    {
        return -1L - (int64_t)u_result;
    }
    else
    {
        return (int64_t)u_result;
    }
}


inline void dec2bit (long long unsigned dec, int size = 64){

    int i;
    long long unsigned aux = 0x8000000000000000Lu;
    for ( i = 0; i < size; i++ ){
        if ( !i ){
            printf("\nSigno:\t\t ");
        }
        if ( !(dec & aux) ){
            printf("0");
        }
        else{
            printf("1");
        }
        if ( !i ){
            printf("\nExponente:\t ");
        }
        if ( i==11 ){
            printf("\nMantisa:\t ");
        }
        aux = aux >> 1Lu;
    }
    return;
}

} //namespace

#endif //_R2BT_BIT_LATTICE_922FN0IWBFIWIF2FI2I2F
