/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_SEARCHING_SET_FACTORY_938UJAIJRFIOEGREAOGHRA
#define _R2BT_SEARCHING_SET_FACTORY_938UJAIJRFIOEGREAOGHRA

namespace r2bt {

template <unsigned DIM, typename Point >
class ss_factory
{
public:
    ss_factory() : sets()
    {
        sets.insert ( Values("Serial",     &searching_set_serial_3::create ) );
        sets.insert ( Values("SerialRec", &searching_set_rec<DIM>::create ) );
        sets.insert ( Values("OneCell",   &searching_set_one_cell<DIM>::create ) );
#ifdef TBB_WAS_FOUND
        sets.insert ( Values("TBB", &searching_set_tbb_3::create ) );
#endif
#ifdef ZMQ_WAS_FOUND
        sets.insert ( Values("zmq", &searching_set_zmq_3<Point>::create ) );
#endif
#ifdef OPENMP_WAS_FOUND
        sets.insert ( Values("OMP", &searching_set_omp_3::create ) );
#endif
    }

    searching_set<DIM>* new_ss(const std::string& name, double h, const std::string& cell_name)
    {
        // Avoid annoying segmentation fault when name is not found
        if ( sets.find(name) == sets.end() )
        {
            throw std::runtime_error("searching set name was not found in factory");
            return nullptr;
        }
        return sets[name](h, cell_name);
    }

private:
    typedef searching_set<DIM>* (*CreatePointer) (double h, const std::string& name) ;
    typedef typename std::unordered_map<std::string, CreatePointer > Map;
    typedef typename Map::value_type Values;

    Map sets;
};

namespace factory {

template <unsigned DIM, typename Point >
inline std::shared_ptr< searching_set<DIM> > createShared(const std::string& name, double h, const std::string& cell_name)
{
    static ss_factory<DIM, Point> factory;
    return std::shared_ptr< searching_set<DIM> >(factory.new_ss(name, h, cell_name));
}

template <unsigned DIM, typename Point >
inline std::unique_ptr< searching_set<DIM> > createUniquePtr(const std::string& name, double h, const std::string& cell_name)
{
    static ss_factory<DIM, Point> factory;
    return std::unique_ptr< searching_set<DIM> >(factory.new_ss(name, h, cell_name));
}

template <unsigned DIM, typename Point >
inline searching_set<DIM>* createRawPointer(const std::string& name, double h, const std::string& cell_name)
{
    static ss_factory<DIM, Point> factory;
    return factory.new_ss(name, h, cell_name);
}

} //namespace factory

} //namespace r2bt

#endif // _R2BT_SEARCHING_SET_FACTORY_938UJAIJRFIOEGREAOGHRA
