/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_NEIGHBORHOOD_OMP_4549846313168
#define _R2BT_NEIGHBORHOOD_OMP_4549846313168

#include "searching_set.hpp"
#include "point_counter.hpp"

#ifdef OPENMP_WAS_FOUND

namespace r2bt {

class searching_set_omp_3 : public point_counter<3>
{
public:
    enum { DIM = 3 };

    searching_set_omp_3( double h, const std::string& name_ ) :
        range( length2range(h) ),
        length ( h ),
        name(name_)
    {
    }

    static searching_set<DIM>* create( double h, const std::string& name )
    {
        return new searching_set_omp_3(h, name);
    }

    virtual ~searching_set_omp_3() { }

    int get_range() override
    {
        return range;
    }

    virtual size_t size() const override
    {
        return domain.size();
    }

    void create_cells() override
    {
        for ( const auto& x : this->counter )
        {
            std::shared_ptr < cell<DIM> > c( factory::create<DIM>(name) );
            typename HashMap::value_type new_value(x.first, c);
            std::pair<typename HashMap::iterator,bool> ok = domain.insert( new_value );
            if ( ok.second )
            {
                typename HashMap::iterator new_value_ite = ok.first;
                new_value_ite->second->reserve( x.second );
                new_value_ite->second->set_limits( x.first.get_origin( range ), length );
            }
            else
            {
                throw std::runtime_error("Cannot insert cell in HashMap?");
            }
        }
    }

    typedef point_adapter<DIM> PointAdapter;

    void insert(const PointAdapter& p) override
    {
        std::array<double, DIM> coord = p.get_coordinates();

        typename HashMap::iterator found = domain.find( p.get_tessel() );

        if ( found != domain.end() )
        {
            found->second->add(coord, p.get_address());
        }
        else
        {
            throw std::runtime_error("Cell should be inserted and initialized previously, create_cells not being called?");
        }
    }

    void build() override
    {
        for ( auto& item : domain )
        {
            item.second->build();
        }
    }

    query_result query(const std::array<double, DIM>& coord, double radius, double sq_radius ) const override
    {
        query_result result;
        throw std::runtime_error("OMP doesn't have a single query method");
        return std::move(result);
    }

    multiquery_result_t multi_query(const std::vector<PointAdapter>& querying, double radius, double sq_radius) const
    {
        multiquery_result_t multi_result( querying.size() );
        long r = (long)( radius / length ) + 1;
        const double inc = exp2(range);
        size_t i;
        const size_t len = querying.size();
        #pragma omp parallel
        {
            multiquery_result_t mr_private( querying.size() / 2 );
            #pragma omp for
            for ( i = 0; i < len; ++i )
            {
                PointAdapter a = querying[i];
                query_result result;
                const std::array<double, DIM>& coord = a.get_coordinates();
                tessel<DIM> center = tessel<DIM>::get_tessel(coord,range);
                tessel<DIM> aux;

                long x, y, z;

                for ( x = -r; x <= r; ++x)
                {
                    if (  x == 0 || ( x > 0 ?
                                    coord[0] + radius > (center.coor[0] + x) * inc :
                                    coord[0] - radius < (center.coor[0] + x + 1) * inc ) )
                    {
                        for ( y = -r; y <= r; ++y )
                        {
                            if (  y == 0 || ( y > 0 ?
                                            coord[1] + radius > (center.coor[1] + y) * inc :
                                            coord[1] - radius < (center.coor[1] + y + 1) * inc ) )
                            {
                                for ( z = -r; z <= r; ++z )
                                {
                                    if (  z == 0 || ( z > 0 ?
                                                    coord[2] + radius > (center.coor[2] + z) * inc :
                                                    coord[2] - radius < (center.coor[2] + z + 1) * inc ) )
                                    {
                                        aux.coor[0] = center.coor[0] + x;
                                        aux.coor[1] = center.coor[1] + y;
                                        aux.coor[2] = center.coor[2] + z;

                                        typename HashMap::const_iterator found = domain.find(aux);
                                        if ( found != domain.end() )
                                        {
                                            query_result q_result = found->second->query(coord, radius, sq_radius);
                                            result.reserve(result.size()+q_result.size());
                                            result.insert(result.end(), q_result.begin(), q_result.end());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                mr_private[ a.get_address() ] =  result;
            } // parallel for
            #pragma omp critical
            multi_result.insert( std::make_move_iterator( mr_private.begin() ), std::make_move_iterator( mr_private.end() ));
        } // pragma parallel

        return std::move(multi_result);
    }


    kquery_result kquery(const std::array<double, DIM>& coord, const size_t K) const override
    {
        kquery_result result;
        tessel<DIM> c = tessel<DIM>::get_tessel(coord,range);

        /* Initial query */
        typename HashMap::const_iterator found = domain.find(c);
        if ( found != domain.end() )
        {
            result = found->second->kquery(coord, K, -1.0 );
        }
        tessel_kquery_3( coord, c, result, K );

        result.resize( K );
        return std::move(result);
    }

    int range;
    double length;
    const std::string name;

private:

    typedef std::shared_ptr< cell<DIM> > CellPtr;
    typedef std::unordered_map<tessel<DIM>, CellPtr> HashMap;

    HashMap domain;

    void tessel_kquery_3( const std::array<double, DIM>& coord, const tessel<DIM>& c, kquery_result& result, const size_t K ) const
    {
        bool keep_going = true;
        double sq_radius = -1.0;
        long x[DIM], r = 0;
        tessel<DIM> aux;
        //LOG_OBJ("Central tessel: " << c << std::endl);
        while ( keep_going )
        {
            r++;
            for ( x[0] = -r; x[0] <= r; ++x[0]  )
            {
                for ( x[1] = -r; x[1] <= r; ++x[1] )
                {
                    for ( x[2] = -r; x[2] <= r; ++x[2] )
                    {
                        if ( abs(x[0]) >= r || abs(x[1]) >= r || abs(x[2]) >= r )
                        {
                            aux.coor[0] = c.coor[0] + x[0];
                            aux.coor[1] = c.coor[1] + x[1];
                            aux.coor[2] = c.coor[2] + x[2];
                            typename HashMap::const_iterator found = domain.find(aux);
                            if ( found != domain.end() )
                            {
                                kquery_result q_result;

                                if ( result.size() < K )
                                {
                                    q_result = found->second->kquery( coord, K, std::numeric_limits<double>::infinity() );
                                }
                                else
                                {
                                    q_result = found->second->kquery(coord, K, sq_radius);
                                }
                                result.merge( q_result );
                                result.resize( K );
                                sq_radius = result.last_sq_distance();
                            }
                        }
                    }
                }
            }

            keep_going = false;
            if ( result.size() < K )
            {
                keep_going = true;
            }
            else // Copiado de Santiago:
            {
                for ( unsigned i = 0; i < DIM; ++i )
                {
                    keep_going |= coord[i] + sqrt(sq_radius) > (c.coor[i] + r) * length;
                    keep_going |= coord[i] - sqrt(sq_radius) < (c.coor[i] - r + 1) * length;
                }
            }
            //LOG_OBJ("Finished kquery (" << coord[0] << " " << coord[1] << " " << coord[2] << ") with sqradius: " << sq_radius << std::endl);
            //LOG_OBJ("Best point was at: " << result.begin()->first << std::endl);
        }
    }
};

} //namespace

#endif // OPENMP_WAS_FOUND

#endif // _R2BT_NEIGHBORHOOD_OMP_4549846313168
