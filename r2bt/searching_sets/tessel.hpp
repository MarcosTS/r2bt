/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef R2BT_TESSEL_HPP_2A4353QT45T
#define R2BT_TESSEL_HPP_2A4353QT45T

#include  <functional>

#include "bit_lattice.hpp"

namespace r2bt
{

template < unsigned DIM >
class tessel
{
public:
    long long coor[DIM];

    inline bool operator==(const tessel& x) const
    {
        unsigned i;
        for ( i = 0; i < DIM; ++i ) {
            if ( coor[i] != x.coor[i] ) return false;
        }
        return true;
    }

    inline bool operator!=(const tessel& x) const
    {
        return ! ( *this == x );
    }

    inline tessel<DIM> operator+(const std::array<int, DIM>& x) const
    {
        tessel<DIM> result;
        for ( size_t i = 0; i < DIM; ++i )
        {
            result.coor[i] = this->coor[i] + x[i];
        }
        return result;
    }

    static inline tessel<DIM> get_tessel(const std::array<double,DIM> &v, int range)
    {
        tessel<DIM> t; unsigned i;
        for ( i = 0; i < DIM; ++i ) {
            t.coor[i] = bit_lattice(v[i], range);
        }
        return t;
    }

    inline std::array<double, DIM> get_origin( int range ) const
    {
        std::array<double, DIM> ar; unsigned i;
        for ( i = 0; i < DIM; ++i ) {
            ar[i] = static_cast<double>( (coor[i])*exp2(range) );
        }
        return ar;
    }
};

#ifdef R2BT_VERBOSE
template < unsigned DIM >
inline std::ostream& operator<<(std::ostream& os, const tessel<DIM>& x)
{
    unsigned i;
    for ( i = 0; i < DIM; ++i ) {
        os << x.coor[i] << " ";
    }
    return os;
}
#endif //R2BT_VERBOSE

} //name space

namespace std
{
    template< unsigned DIM > struct hash< r2bt::tessel<DIM> >
    {
        typedef r2bt::tessel<DIM> argument_type;
        typedef std::size_t result_type;
        result_type operator()(argument_type const& k) const
        {
            std::hash<long long> hash_f;
            size_t h = 0;

            h =  hash_f(k.coor[0]);
            // Magic Number and Expression adapted from boost::hash_combine
            unsigned i;
            for ( i = 1; i < DIM; ++i ) {
                h ^= hash_f(k.coor[i]) + 0x9e3779b9 + (h<<6) + (h>>2);
            }
            return h;
        }
    };
}

#endif //R2BT_TESSEL_HPP_2A4353QT45T
