/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_NEIGHBORHOOD_IMP_TQWSTYWGTRYHSCDFY34UJE5YW45YW432EG
#define _R2BT_NEIGHBORHOOD_IMP_TQWSTYWGTRYHSCDFY34UJE5YW45YW432EG

#include "searching_set.hpp"
#include "point_counter.hpp"

// #define NOT_STD_HASH
// #define ALTERNATIVE_HASH poner aquí el nuevo hash

namespace r2bt {

class searching_set_serial_3 : public point_counter<3>
{
public:

    enum { DIM = 3 };

    searching_set_serial_3( double h, const std::string& name_ ) :
        range( length2range(h) ),
        length ( h ),
        name(name_)
    {
    }

    static searching_set<DIM>* create( double h, const std::string& name )
    {
        return new searching_set_serial_3(h, name);
    }

    virtual ~searching_set_serial_3() { }

    int get_range() override
    {
        return range;
    }

    virtual size_t size() const override
    {
        return domain.size();
    }

    void create_cells() override
    {
        for ( const auto& x : this->counter )
        {
            std::shared_ptr < cell<DIM> > c( factory::create<DIM>(name) );
            typename HashMap::value_type new_value(x.first, c);
            std::pair<typename HashMap::iterator,bool> ok = domain.insert( new_value );
            if ( ok.second )
            {
                typename HashMap::iterator new_value_ite = ok.first;
                const tessel<DIM>& t = new_value_ite->first;
                LOG_OBJ("Adding cell to searching set with tessel: " << t << std::endl);
                new_value_ite->second->reserve( x.second );
                // Cell origin is the tessel * 2^range in each coordinate, the cell side is the 2^range
                new_value_ite->second->set_limits( t.get_origin(range), exp2(range));
            }
            else
            {
                throw std::runtime_error("Cannot insert cell in HashMap?");
            }
        }
    }

    typedef point_adapter<DIM> PointAdapter;

    void insert(const PointAdapter& p) override
    {
        std::array<double, DIM> coord = p.get_coordinates();

        typename HashMap::iterator found = domain.find( p.get_tessel() );

        if ( found != domain.end() )
        {
            LOG("Adding %lx | %lu to ", p.get_address(), p.get_address());
            LOG_OBJ( found->first << std::endl);
            found->second->add(coord, p.get_address());
        }
        else
        {
            throw std::runtime_error("Cell should be inserted and initialized previously, create_cells not being called?");
        }
    }

    void build() override
    {
        for ( auto& item : domain )
        {
            item.second->build();
        }
    }

    static inline double radius_limit( const kquery_result& result, size_t K, const double& radius )
    {
        return result.size() < K ? std::numeric_limits<double>::infinity() : radius;
    }

    kquery_result kquery(const std::array<double, DIM>& coord, size_t K) const override
    {
        kquery_result result;
        const multi_cell<DIM> mc(range);
        const tessel<DIM> center = tessel<DIM>::get_tessel(coord, range);

        int r = 0;
        bool keep_going = true;
        double sq_radius = std::numeric_limits<double>::infinity();

        while ( keep_going )
        {
            LOG_OBJ("kquery looking cells using r = " << r << " have " << result.size() << " points needs " << K << std::endl);
            LOG_OBJ("               sq_radius = " << sq_radius << std::endl);
            const std::forward_list < std::array<int, DIM> >& increment_list = multi_cell<DIM>::near_cells_3(r);

            keep_going = false;

            for ( const std::array<int, DIM>& increment : increment_list )
            {
                if ( mc.filter(coord, sqrt(radius_limit(result, K, sq_radius)), center, increment) )
                {
                    tessel<DIM> aux = center + increment;
                    typename HashMap::const_iterator found = domain.find(aux);
                    if ( found != domain.end() )
                    {
                        LOG_OBJ("kquery looking in cell " << aux << std::endl);
                        kquery_result q_result = found->second->kquery( coord, K, radius_limit(result, K, sq_radius) );
                        result.merge(q_result);
                        result.resize( K );
                        sq_radius = result.last_sq_distance();
                        LOG_OBJ("               sq_radius = " << sq_radius << " (new) " << std::endl);
                    }
                    keep_going = true;
                }
                else
                {
                    LOG_OBJ(" cell " << center + increment << " was filter out " << std::endl);
                }
            }
            ++r;
        }
        return std::move(result);
    }

    query_result query(const std::array<double, DIM>& coord, double radius, double sq_radius) const override
    {
        query_result result;
        multi_cell<DIM> mc(range);
        std::forward_list< tessel<DIM> > tessel_list = mc.select( coord, radius );

        for ( auto& aux : tessel_list )
        {
            typename HashMap::const_iterator found = domain.find(aux);
            if ( found != domain.end() )
            {
                query_result q_result = found->second->query(coord, radius, sq_radius);
                result.reserve(result.size()+q_result.size());
                result.insert(result.end(), q_result.begin(), q_result.end());
            }
        }
        return std::move(result);
    }

    int range;
    double length;
    const std::string name;

private:

    typedef std::shared_ptr< cell<DIM> > CellPtr;
#ifdef NOT_STD_HASH 
    typedef std::unordered_map<tessel<DIM>, CellPtr, ALTERNATIVE_HASH> HashMap;
#else
    typedef std::unordered_map<tessel<DIM>, CellPtr> HashMap;
#endif

    HashMap domain;
};

} //namespace

#endif // _R2BT_NEIGHBORHOOD_IMP_TQWSTYWGTRYHSCDFY34UJE5YW45YW432EG
