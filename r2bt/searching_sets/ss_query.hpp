/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_SS_QUERY_1243784012947192475124
#define _R2BT_SS_QUERY_1243784012947192475124

namespace r2bt {

template<typename Iterator, typename Neighborhood, typename FunctorCoordinates>
multiquery_result_t query(Neighborhood* neigh,
                          Iterator first, Iterator last, size_t size_,
                          FunctorCoordinates point2coordinates, // Convert a querying point to std::array
                          double radius)
{
    return std::move( query(*neigh, first, last, size_, point2coordinates, radius) );
}

template<typename Iterator, typename Neighborhood, typename FunctorCoordinates>
multiquery_result_t query(Neighborhood& neigh,
                          Iterator first, Iterator last, size_t size_,
                          FunctorCoordinates point2coordinates, // Convert a querying point to std::array
                          double radius)
{
    typedef typename std::iterator_traits<Iterator>::value_type QueryPoint;
    typedef typename std::iterator_traits<Iterator>::iterator_category iterator_tag;
    typedef typename std::remove_pointer<QueryPoint>::type QueryPointWitoutPointer;
    typedef point_adapter<Neighborhood::dim> Adapter;

    LOG_OBJ("Serial querying for N points, radius is: " << radius << "\n");

    if ( radius < 0 )
    {
        throw std::runtime_error("radius could not be less than zero");
    }

    const double sq_radius = radius*radius;
    Iterator ite;

    std::vector<Adapter> vector_adapter;
    vector_adapter.reserve(size_);

    for ( ite = first ; ite != last; ++ite )
    {
        Adapter a;
        set_coordinates_and_tessel<Adapter,3>( a, neigh.get_range(), point2coordinates(*ite) );
        set_address<Adapter, QueryPointWitoutPointer>( a, *ite );
        compute_diff<Adapter, Iterator>( a, first, ite, iterator_tag{} );
        vector_adapter.push_back( a );
    }

    return neigh.multi_query(vector_adapter, radius, sq_radius);
}

template<typename Iterator, typename Neighborhood, typename RadiusIterator, typename FunctorCoordinates>
multiquery_result_t query(Neighborhood& neigh,
                          Iterator first, Iterator last, size_t size_,
                          FunctorCoordinates point2coordinates,
                          RadiusIterator first_radius, RadiusIterator last_radius)
{
    typedef typename std::iterator_traits<Iterator>::value_type QueryPoint;
    typedef typename std::iterator_traits<Iterator>::iterator_category iterator_tag;
    typedef typename std::remove_pointer<QueryPoint>::type QueryPointWitoutPointer;
    typedef point_adapter<Neighborhood::dim> Adapter;

    LOG_OBJ("Serial querying for N points, first radius is: " << *first_radius << "\n");

    if ( *first_radius < 0 )
    {
        throw std::runtime_error("radius could not be less than zero");
    }

    std::unordered_map< uintptr_t, query_result > result;

    for ( Iterator ite = first ; ite != last; ++ite )
    {
        Adapter a;
        set_coordinates_and_tessel<Adapter,3>( a, neigh.get_range(), point2coordinates(*ite) );
        set_address<Adapter, QueryPointWitoutPointer>( a, *ite );
        compute_diff<Adapter, Iterator>( a, first, ite, iterator_tag{} );
        result[ a.get_address() ] =  neigh.query( a.get_coordinates(), *first_radius );

        // sanity check
        if ( (first_radius + 1) != last_radius ) {
            ++first_radius;
        }
    }

    return std::move( result );
}

} //namespace

#endif // _R2BT_SS_QUERY_1243784012947192475124
