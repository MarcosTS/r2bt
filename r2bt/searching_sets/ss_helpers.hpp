/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_SS_ITERATOR_DIFF_2629EG8RASLKGHR
#define _R2BT_SS_ITERATOR_DIFF_2629EG8RASLKGHR

namespace r2bt {

template<typename Adapter, typename Iterator>
void compute_diff( Adapter& a, Iterator first, Iterator ite, std::input_iterator_tag )
{
    // Does nothing!
}

template<typename Adapter, typename Iterator>
void compute_diff( Adapter& a, Iterator first, Iterator ite, std::bidirectional_iterator_tag )
{
    // Does nothing!
}

template<typename Adapter, typename Iterator>
void compute_diff( Adapter& a, Iterator first, Iterator ite, std::random_access_iterator_tag )
{
    uintptr_t diff = ite - first;
    a.update_reference(diff);
}

template<typename Adapter, unsigned DIM>
void set_coordinates_and_tessel( Adapter& a, int range, const std::array<double, DIM> &coord )
{
    a.update_coordinates_tessel(coord, range);
}

template<typename Adapter, unsigned DIM>
void set_coordinates( Adapter& a, const std::array<double, DIM> &coord )
{
    a.update_coordinates(coord);
}

template<typename Adapter, typename Point>
void set_address( Adapter& a, const Point& p )
{
    a.update_reference( reinterpret_cast<uintptr_t>(&p) );
}

template<typename Adapter, typename Point>
void set_address( Adapter& a, const Point* p )
{
    a.update_reference( reinterpret_cast<uintptr_t>(p) );
}


} //namespace

#endif // _R2BT_SS_INSERT_23503U45TWEJGFR5
