/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_SEARCHING_SET_ONE_CELL_92385QR8U48
#define _R2BT_SEARCHING_SET_ONE_CELL_92385QR8U48

namespace r2bt {

template < unsigned DIM >
class searching_set_one_cell : public searching_set<DIM>
{
public:
    searching_set_one_cell( double h, const std::string& name_ ) :
        range( length2range(h) ),
        length ( h ),
        name(name_),
        number_of_points ( 0 )
    {
    }

    static searching_set<DIM>* create( double h, const std::string& name )
    {
        return new searching_set_one_cell(h, name);
    }

    virtual ~searching_set_one_cell() { }

    void count_point( const tessel<DIM>& ) override
    {
        ++number_of_points;
    }

    int get_range() override
    {
        return range;
    }

    void create_cells() override
    {
        the_cell = CellPtr( factory::create<DIM>(name) );
        the_cell->set_limits(origin, length );
        the_cell->reserve( number_of_points );
    }

    typedef point_adapter<DIM> PointAdapter;

    void insert(const PointAdapter& p) override
    {
        std::array<double, DIM> coord = p.get_coordinates();
        the_cell->add(coord, p.get_address());
    }

    void build() override
    {
        the_cell->build();
    }

    kquery_result kquery(const std::array<double, DIM>& coord, size_t K) const override
    {
        kquery_result q_result = the_cell->kquery(coord, K, -1.0);
        return std::move(q_result);
    }

    query_result query(const std::array<double, DIM>& coord, double radius, double sq_radius) const override
    {
        query_result result = the_cell->query(coord, radius, sq_radius);
        return std::move(result);
    }

    size_t size() const
    {
        return 1;
    }

    int range;
    double length;
    const std::string name;
    size_t number_of_points;

private:

    typedef std::unique_ptr< cell<DIM> > CellPtr;

    CellPtr the_cell;

    std::array<double, 3> origin;
};

} // namespace

#endif // _R2BT_SEARCHING_SET_ONE_CELL_92385QR8U48
