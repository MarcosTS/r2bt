/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_CELL_DEBUGS_1059810JTQP30AFAJAWOJFER
#define _R2BT_CELL_DEBUGS_1059810JTQP30AFAJAWOJFER

namespace r2bt
{

bool compare_with_address(const distance_and_ref &a, const distance_and_ref &b)
{
    return a.address < b.address;
}

enum MATCH_RESULT { EQUAL_RESULT, SIZE_MISMATCH, MISMATCH };

MATCH_RESULT matching_results( query_result& lhs, query_result& rhs )
{
    if ( lhs.size() != rhs.size() ) return SIZE_MISMATCH;

    std::sort(lhs.begin(), lhs.end());
    std::sort(rhs.begin(), rhs.end());

    if ( lhs != rhs) return MISMATCH;
    else return EQUAL_RESULT;
}

MATCH_RESULT matching_results( kquery_result& lhs, kquery_result& rhs )
{
    auto length1 = std::distance( lhs.begin(), lhs.end() );
    auto length2 = std::distance( rhs.begin(), rhs.end() );
    if ( length1 != length2 ) return SIZE_MISMATCH;

    lhs.sort( compare_with_address );
    rhs.sort( compare_with_address );

    if ( lhs != rhs) return MISMATCH;
    else return EQUAL_RESULT;
}

template <typename AnyPoint>
void print(std::ostream& os, const kquery_result &result)
{
    for ( const distance_and_ref& dr : result )
    {
        const AnyPoint* p = (AnyPoint*)(dr.address);
        os << *p << " sq_dist: " << dr.sq_distance <<  std::endl;
    }
}

template <typename AnyPoint>
void print(std::ostream& os, const query_result &result)
{
    for ( const auto& address : result )
    {
        const AnyPoint* p = (AnyPoint*)(address);
        os << *p << std::endl;
    }
}

} // namespace

#endif // _R2BT_CELL_DEBUGS_1059810JTQP30AFAJAWOJFER
