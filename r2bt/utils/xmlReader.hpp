/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _XMLREADER_92HJF02J02JF9209FJ
#define _XMLREADER_92HJF02J02JF9209FJ

#include "example_point.hpp"

#include <sys/stat.h>

#include <vector>
#include <cstdio>

#include <iostream>
#include <sstream>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

namespace r2bt {

xmlDocPtr parserDoc(const char *content, int length)
{
    xmlDocPtr doc = xmlReadMemory(content, length, "points.xml", NULL, 0);
    if (doc == NULL)
    {
        fprintf(stderr, "Failed to parse document\n");
        return NULL;
    }
    return doc;
}

template < unsigned DIM >
void read_vector( std::istream& reading, std::vector< example_point<DIM> >& v)
{
    unsigned i; double a_point[DIM];
    for ( i = 0; i < DIM; ++i ) { reading >> a_point[i]; }
    while ( reading.good() )
    {
        v.push_back( example_point<DIM>(a_point) );
        for ( i = 0; i < DIM; ++i ) { reading >> a_point[i]; }
    }
}

template < unsigned DIM >
void process_points(xmlNodeSetPtr nodes, std::vector< example_point<DIM> >& v)
{
    int size = (nodes) ? nodes->nodeNr : 0;
    if ( size == 1 )
    {
        xmlNodePtr p = nodes->nodeTab[0];

        std::string content((const char*)xmlNodeGetContent(p));

        std::stringstream reading(content);

        read_vector( reading, v);
    }
    else
    {
        fprintf(stderr, "Error: Points not found\n");
    }
}

template < unsigned DIM >
int find_xpath_expression(xmlDocPtr doc, const xmlChar* xpathExpr, std::vector< example_point<DIM> >& v)
{
    xmlXPathContextPtr xpathCtx;
    xmlXPathObjectPtr xpathObj;

    /* Create xpath evaluation context */
    xpathCtx = xmlXPathNewContext(doc);
    if(xpathCtx == NULL)
    {
        fprintf(stderr,"Error: unable to create new XPath context\n");
        return(-1);
    }

    /* Evaluate xpath expression */
    xpathObj = xmlXPathEvalExpression(xpathExpr, xpathCtx);
    if(xpathObj == NULL)
    {
        fprintf(stderr,"Error: unable to evaluate xpath expression \"%s\"\n", xpathExpr);
        xmlXPathFreeContext(xpathCtx);
        return(-1);
    }

    // Read points
    process_points( xpathObj->nodesetval, v );

    /* Cleanup */
    xmlXPathFreeObject(xpathObj);
    xmlXPathFreeContext(xpathCtx);

    return(0);
}

template < unsigned DIM >
int point_loader( std::vector< example_point<DIM> >& v, const char* filename)
{
    FILE* g; char *document;
    struct stat st; size_t size;

    if ( stat(filename, &st) == -1 )
    {
        return 1;
    }

    stat(filename, &st);
    size = st.st_size;

    g = fopen(filename, "rb");
    document = (char*)malloc( size + 1 );

    size_t result = fread( document, sizeof(char), size, g );
    document[size] = '\0';

    if ( result != size )
    {
        fprintf(stderr,"Error: unable to read the file completely '%s'\n", filename);
    }
    else
    {
#ifdef R2BT_VERBOSE
        fprintf(stderr,"Info: '%s' reading file completed\n", filename);
#endif // R2BT_VERBOSE
    }

    /* Process document VTKFile */
    xmlDoc *doc = parserDoc(document, size);

    /*Get the root element node */
    //xmlNode *root_element = xmlDocGetRootElement(doc);

    find_xpath_expression(doc, (const xmlChar*)"/VTKFile/UnstructuredGrid/Piece/Points/DataArray", v);
#ifdef R2BT_VERBOSE
    std::cerr << "V: size = " << v.size() << "\n";
    std::cerr << v[0] << "\n";
    std::cerr << v[v.size()-1] << "\n\n";
#endif // R2BT_VERBOSE

    xmlFreeDoc(doc);

    xmlCleanupParser();

    return 0;
}

void load_file(const char *filename, std::vector< example_point<3> >& v)
{
    if ( 0 != point_loader( v, filename ) )
    {
        std::cerr << "Can't find: '" << filename << "'" << std::endl;
    }
}

} //name space

#endif // _XMLREADER_92HJF02J02JF9209FJ

