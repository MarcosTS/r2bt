/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef MSH_READER_JIGSAW_52356234523324523
#define MSH_READER_JIGSAW_52356234523324523

#include "example_point.hpp"

#include <vector>
#include <cstdio>
#include <cstring>

namespace r2bt {

template < unsigned DIM >
int read_vector_JIGSAW( FILE* file, std::vector< example_point<DIM> >& v)
{
    char line[256];
    char key[256]; long unsigned size = 0;
    long unsigned i = 0, j;

    char *aux = fgets(line, 256, file);
    int res_sscanf = sscanf(line,"%[^=]=%lu", key, &size);

    while ( (aux != NULL) && (i < 10) ) {
        //fprintf(stderr, "Skipping: '%s'\n", line);
        if ( (line[0] != '#') && (res_sscanf == 2) && (strcmp(key, "POINT") == 0) )
        {
            break;
        }
        ++i;
        aux = fgets(line, 256, file);
        res_sscanf = sscanf(line,"%[^=]=%lu", key, &size);
    }

    if ( ! ((res_sscanf == 2) && (strcmp(key, "POINT") == 0) ) )
    {
        fprintf(stderr, "Unable to read number of points\n");
        return 1;
    }
    fprintf(stderr, "Number of points: %lu\n", size);

    v.reserve(size);
    double a_point[3];
    i = 0;

    while ( fgets(line, 256, file) && (i < size) && (sscanf(line, "%lf ; %lf ; %lf ; %lu", a_point, a_point+1, a_point+2, &j) == 4) ) {
        v.push_back( example_point<DIM>(a_point, i) );
        ++i;
    }

    if ( i != size ) {
        fprintf(stderr, "Error: Unexpected format in point %lu, could not read the point\n", i);
        fprintf(stderr, "Line was: '%s'\n", line);
        return 2;
    }
    else {
        fprintf(stderr, "Success: %lu points read from file\n", size);
    }

    return 0;
}

void load_msh_JIGSAW_file(const char *filename, std::vector< example_point<3> >& v)
{
    FILE *file = fopen( filename, "r");
    if ( file != NULL ) {
        read_vector_JIGSAW(file, v);
        fclose(file);
    }
    else {
        fprintf(stderr, "Can't find: '%s'\n", filename);
    }
}

} //name space

#endif // MSH_READER_JIGSAW_52356234523324523

