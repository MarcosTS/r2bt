/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_COLLECTOR_ZMQ_0Q389U4H4P90QAHQOH89
#define _R2BT_COLLECTOR_ZMQ_0Q389U4H4P90QAHQOH89

#define THIS_FILE get_filename(__FILE__)

class collector {
public:
    collector( zmq::context_t& c, std::atomic_int& nr  )
        : context(c)
        , number_of_request (nr)
        , thread(new std::thread( &collector::do_work, std::ref(*this)))
    {
    }

    virtual ~collector()
    {
    }

    r2bt::multiquery_result_t multi_collect() {
        thread->join();
        return std::move(result);
    }

    void do_work()
    {
        zmq::socket_t the_collector (context, ZMQ_PULL);
        the_collector.bind(COLLECT_END_POINT);
        LOG_OBJ("bound to " << COLLECT_END_POINT << " in collector::do_work" << std::endl);

        while ( number_of_request > 0 )
        {
            try
            {
                zmq::message_t result_msg;
                the_collector.recv(&result_msg, 0);

                msg_header h;
                r2bt::query_result neigh_refs;

                get_result_message( result_msg, h, neigh_refs);

                LOG("collector received message sized: %u\n", result_msg.size());
                LOG("collector received result message %u neighbors for %lx point\n", h.number_of, h.address);

                if ( h.number_of > 0 )
                {
                    typename r2bt::multiquery_result_t::iterator found = result.find(h.address);
                    if ( found != result.end() )
                    {
                        LOG("Add result to existing point %p\n", h.address);
                        r2bt::query_result& v = found->second;
                        v.reserve(v.size()+h.number_of);
                        v.insert( v.end(), neigh_refs.begin(), neigh_refs.end() );
                    }
                    else
                    {
                        LOG("Add result to new point %p\n", h.address);
                        result[h.address] = std::move(neigh_refs);
                    }
                }
            }
            catch ( zmq::error_t ex )
            {
                LOG("Fails socket received %s in %s:%u\n", ex.what(), THIS_FILE, __LINE__ );
                break;
            }
            --number_of_request;
            LOG_OBJ("Collector: number of request = " << number_of_request << std::endl);
        }
    }

    zmq::context_t& context;
    std::atomic_int& number_of_request;
    std::unique_ptr<std::thread> thread;
    r2bt::multiquery_result_t result;
};


#endif // _R2BT_NEIGHBORHOOD_ZMQ_32545034905IJQGWQ3P90JSGDG
