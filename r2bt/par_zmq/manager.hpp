/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 * 
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef ZMQ_MANAGER_2QW409UW4OLJIL3425J1QH
#define ZMQ_MANAGER_2QW409UW4OLJIL3425J1QH

#include <iostream>
#include <stdio.h>
#include <zmq.hpp>

#include "defines.hpp"

#define THIS_FILE get_filename(__FILE__)

/* Emit as many messages as cores, them send more
when it receives 'done' messages */

class manager
{
public:
    manager(zmq::context_t& c, unsigned cores_)
        : context(c)
        , thread(new std::thread( &manager::do_work, std::ref(*this)))
        , cores(cores_)
    {
    }

    manager() = delete;
    manager(const manager&) = delete;
    manager(manager&&) = delete;

    virtual ~manager()
    {
        LOG_OBJ("manager waiting for thread to finish (destructor)\n");
        thread->join();
        LOG_OBJ("manager destroyed\n");
    }

    void do_work()
    {
        zmq::socket_t sender(context, ZMQ_PAIR);
        zmq::socket_t puller(context, ZMQ_PULL);

        sender.connect(MANAGER_END_POINT);
        LOG_OBJ("connected to " << MANAGER_END_POINT << " in manager::do_work" << std::endl);

        puller.bind(MANAGER_PULLER_POINT);
        LOG_OBJ("bound to " << MANAGER_PULLER_POINT << " in manager::do_work" << std::endl);

        bool on_success = true;

        try
        {
            for ( unsigned i = 0; i < cores; ++i )
            {
                sender.send("W", 1, 0);
            }
        }
        catch ( zmq::error_t ex ) {
            LOG("manager throws exception %s in %s:%u\n", ex.what(), THIS_FILE, __LINE__ );
            on_success = false;
        }

        while ( on_success )
        {
            try
            {
                zmq::message_t msg;
                puller.recv(&msg, 0);
                sender.send("W", 1, 0);
            }
            catch ( zmq::error_t ex )
            {
                LOG("manager throws exception %s in %s:%u\n", ex.what(), THIS_FILE, __LINE__ );
                on_success = false;
            }
        }

        LOG("manager closing sockets do_work %s:%u\n", THIS_FILE, __LINE__ );

        puller.close();
        sender.close();

        LOG("manager finished do_work %s:%u\n", THIS_FILE, __LINE__ );
    }

    zmq::context_t& context;
    std::unique_ptr<std::thread> thread;
    unsigned cores;
};

#endif // ZMQ_MANAGER_2QW409UW4OLJIL3425J1QH
