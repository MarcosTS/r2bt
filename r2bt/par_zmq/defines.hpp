/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 * 
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef ZMQ_DEFINES_34UWAOWWTUW09
#define ZMQ_DEFINES_34UWAOWWTUW09

#include <zmq.hpp>

#define COLLECT_END_POINT "inproc://collect"
#define PRODUCE_END_POINT "inproc://produce"

#define MANAGER_END_POINT "inproc://manager"
#define MANAGER_PULLER_POINT "inproc://puller"

#define MESSAGE_TO_FINISH "finish"
#define MESSAGE_TO_ACKNOWLEDGE "ack"

#endif // ZMQ_DEFINES_34UWAOWWTUW09
