/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_NEIGHBORHOOD_ZMQ_32545034905IJQGWQ3P90JSGDG
#define _R2BT_NEIGHBORHOOD_ZMQ_32545034905IJQGWQ3P90JSGDG

#ifdef ZMQ_WAS_FOUND

#include <thread>
#include <zmq.hpp>

#include "worker.hpp"
#include "manager.hpp"
#include "collector.hpp"

#define THIS_FILE get_filename(__FILE__)

namespace r2bt {

template < typename Point >
class searching_set_zmq_3 : public r2bt::point_counter<3, Point>
{
public:
    enum { DIM = 3 };

    searching_set_zmq_3( r2bt::cell<DIM> *c, int r )
        : proto_cell(c)
        , range(r)
        , context(1)
        , the_manager (context, 8) // TODO find number of cores
        , manager_socket (context, ZMQ_PAIR)
    {
        manager_socket.bind(MANAGER_END_POINT);
        LOG("bound to %s at creating searching_set_zmq\n", MANAGER_END_POINT);
    }

    virtual ~searching_set_zmq_3()
    {
        LOG_OBJ("closing sockets in searching_set_zmq destructor\n");
        manager_socket.close();

        LOG_OBJ("closing workers in searching_set_zmq destructor\n");
        for ( typename HashMap::value_type& x : domain ) {
            x.second->close();
        }

        LOG_OBJ("closing context in searching_set_zmq destructor\n");
        context.close();
    }

    int get_range() override
    {
        return range;
    }

    virtual size_t size() const override
    {
        return domain.size();
    }

    void create_cells() override
    {
        throw std::runtime_error("create_cells not implemented in " __FILE__);
    }

    typedef r2bt::point_adapter<DIM, Point> PointAdapter;

    void insert(const PointAdapter& p) override
    {
        std::array<double, DIM> coord = p.get_coordinates();

        typename HashMap::iterator found = domain.find(r2bt::tessel<DIM>::get_tessel(coord,range));

        if ( found != domain.end() )
        {
            LOG("Adding %p to existing cell ", p.get_address());
            LOG_OBJ( found->first << std::endl);
            found->second->the_cell->add(coord, p.get_address());
        }
        else
        {
            LOG_OBJ("Adding to new cell\n");
            std::shared_ptr< worker<DIM> > ptr(new worker<DIM>(context, proto_cell));
            typename HashMap::value_type x( r2bt::tessel<DIM>::get_tessel(coord,range), ptr);
            std::pair<typename HashMap::iterator,bool> ok = domain.insert( x );
            if ( ok.second )
            {
                LOG("Adding %p to ", p.get_address());
                LOG_OBJ( ok.first->first << std::endl);
                ok.first->second->the_cell->add(coord, p.get_address());
                ok.first->second->init();
            }
            else
            {
                throw std::runtime_error("Cannot insert element in HashMap?");
            }
        }
        LOG_OBJ("Point added to cell\n");
    }

    void build() override
    {
        for ( auto& item : domain )
        {
            item.second->the_cell->build();
        }
    }

    void multi_query(const PointAdapter& p, double radius, double sq_radius, std::atomic_int& number_of_request) const
    {
        LOG("Begins multi querying, point: [ %f, ... ] radius: %f\n", p.get_coordinates()[0], radius);

        const r2bt::tessel<DIM> c = r2bt::tessel<DIM>::get_tessel(p.get_coordinates(),range);
        const double inc = exp2(range);
        const long r = (long)(radius / inc) + 1;

        query_result result;
        long x,y,z;
        for ( x = -r; x <= 1+r; ++x )
        {
            if ( x == 0 || ( x > 0 ?
                            p.get_coordinates()[0] + radius > (c.coor[0] + x) * inc :
                            p.get_coordinates()[0] - radius < (c.coor[0] + x + 1) * inc ) )
            {
                for ( y = -r; y <= 1+r; ++y )
                {
                    if ( y == 0 || ( y > 0 ?
                                    p.get_coordinates()[1] + radius > (c.coor[1] + y) * inc :
                                    p.get_coordinates()[1] - radius < (c.coor[1] + y + 1) * inc ) )
                    {
                        for ( z = -r; z <= 1+r; ++z )
                        {
                            if ( z == 0 || ( z > 0 ?
                                            p.get_coordinates()[2] + radius > (c.coor[2] + z) * inc :
                                            p.get_coordinates()[2] - radius < (c.coor[2] + z + 1) * inc ) )
                            {
                                tessel<DIM> aux;
                                aux.coor[0] = c.coor[0] + x;
                                aux.coor[1] = c.coor[1] + y;
                                aux.coor[2] = c.coor[2] + z;
                                LOG("Point [ %7.3f, %7.3f, %7.3f ]", p.get_coordinates()[0], p.get_coordinates()[1], p.get_coordinates()[2]);
                                LOG_OBJ(" radius: " << radius << " looking in cell: " << aux << std::endl);

                                typename HashMap::const_iterator found = domain.find(aux);
                                if ( found != domain.end() )
                                {
                                    ++number_of_request;
                                    LOG_OBJ("multi_query: number of request = " << number_of_request << std::endl);
                                    zmq::message_t msg;
                                    zmq::socket_t& m = const_cast<zmq::socket_t&>(manager_socket);
                                    m.recv(&msg, 0);
                                    found->second->query( p.get_address(), p.get_coordinates(), sq_radius);
                                }
                            }
                        }
                    }
                }
            }
        }
        LOG("Ended multi querying, point: [ %f, ... ] radius: %f\n", p.get_coordinates()[0], radius);
    }

    query_result query(const std::array<double,3>& v, double radius) const override
    {
        throw std::runtime_error("Individual query not implemented...");
        query_result result;
        return std::move(result);
    }

    kquery_result kquery(const std::array<double, DIM>& /*p*/, size_t /*K*/) const override
    {
        throw std::runtime_error("Individual kquery not implemented...");
        kquery_result result;
        return std::move(result);
    }

    typedef std::unordered_map<r2bt::tessel<DIM>, std::shared_ptr<worker<DIM> > > HashMap;

    r2bt::cell<DIM>* proto_cell;
    HashMap domain;
    int range;

    zmq::context_t context;

    manager the_manager;
    zmq::socket_t manager_socket;
};

template<typename Iterator, typename PointAdapter>
multiquery_result_t query(searching_set_zmq_3<PointAdapter>& neigh, double radius, Iterator first, Iterator last)
{
    LOG_OBJ("Querying for N points at once (parallel): \n");
    if ( radius < 0 )
    {
        throw std::runtime_error("radius could not be less than zero");
    }

    std::atomic_int number(1);
    LOG_OBJ("multi_query iteration: number of request = " << number << std::endl);

    double sq_radius = radius*radius;

    /* Executes the thread for collection results */
    collector the_collector(neigh.context, number);

    for ( ; first != last; ++first )
    {
        typedef typename std::iterator_traits<Iterator>::value_type QueryPoint;
        typedef r2bt::point_adapter<3, QueryPoint> Adapter;

        Adapter a( *first, neigh.get_range() );
        neigh.multi_query( a, radius, sq_radius, number);
    }

    --number;
    LOG_OBJ("multi_query iteration: number of request = " << number << std::endl);

    // In some cases the collector ends before the multiquery, and so it does not see number 0
    if ( number == 0 )
    {
        // Send useless result to collector to end it
        zmq::socket_t pusher(neigh.context, ZMQ_PUSH);
        pusher.connect(COLLECT_END_POINT);
        LOG_OBJ("connected to " << COLLECT_END_POINT << " in query iteration" << std::endl);

        zmq::message_t msg;
        msg_header h;
        h.address = 0;
        h.number_of = 0;

        set_result_message( msg, h, nullptr);

        LOG_OBJ("query iteration sends empty " << std::endl);
        pusher.send( msg, 0);
    }

    /* Wait until result is completed (collector thread exits) */
    return std::move( the_collector.multi_collect() );
}

} //name space

#endif // ZMQ_WAS_FOUND

#endif // _R2BT_NEIGHBORHOOD_ZMQ_32545034905IJQGWQ3P90JSGDG
