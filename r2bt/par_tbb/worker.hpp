/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef TBB_WORKER_340U9Q034TU
#define TBB_WORKER_340U9Q034TU

//#include "defines.hpp"
#include "msg_types.hpp"

#define THIS_FILE get_filename(__FILE__)

namespace par_tbb {

template <unsigned DIM>
class worker
{
public:
    worker( graph &g, const std::string& name)
        : the_cell( factory::create<DIM>(name) )
        , node(g, tbb::flow::unlimited, do_work(the_cell.get()))
        , node_for_k_query(g, 1, do_k_query(the_cell.get()))
    {
    }

    worker() = delete;
    worker(const worker&) = delete;
    worker(worker&&) = delete;

    virtual ~worker()
    {
    }

    struct do_work
    {
        do_work(cell<DIM>* c)
            : the_cell(c)
        {
        }

        par_tbb::msg_result operator() (const par_tbb::msg_data<DIM> &msg) const
        {
            par_tbb::msg_result res;
            res.neigh_refs = the_cell->query(msg.point, msg.radius, msg.sq_radius);
            res.address = msg.address;
            return std::move(res);
        }

        cell<DIM>* the_cell;
    };

    struct do_k_query
    {
        do_k_query(cell<DIM>* c)
            : the_cell(c)
        {
        }

        static inline double radius_limit( const std::shared_ptr< kquery_result >& result, size_t K, const double& radius )
        {
            return result->size() < K ? std::numeric_limits<double>::infinity() : radius;
        }

        msg_K_data_and_result operator() (const msg_K_data_and_result& msg) const
        {
            LOG_OBJ("worker doing kquery" << std::endl);
            msg_K_data_and_result res = msg;

            kquery_result q_result = the_cell->kquery( msg.point , msg.K, radius_limit(res.result, msg.K, msg.sq_radius) );
            res.result->merge(q_result);
            res.result->resize( res.K );
            res.sq_radius = res.result->last_sq_distance();

            return res;
        }

        cell<DIM>* the_cell;
    };


    std::unique_ptr< cell<DIM> > the_cell;
    function_node<par_tbb::msg_data<DIM>, par_tbb::msg_result> node;
    function_node<msg_K_data_and_result,msg_K_data_and_result> node_for_k_query;
};

}// namespace par_tbb

#endif // TBB_WORKER_340U9Q034TU
