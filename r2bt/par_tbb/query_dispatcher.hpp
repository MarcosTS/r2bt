/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_QUERY_DISPATCHER_TBB_2354792387529375923
#define _R2BT_QUERY_DISPATCHER_TBB_2354792387529375923

#define THIS_FILE get_filename(__FILE__)

namespace par_tbb {

// TODO: think about this class and how to route msg in flow-graph
// This class is not being used!!
class query_dispatcher {
public:
    enum { DIM = 3 };
    typedef std::unordered_map<tessel<DIM>, std::shared_ptr<par_tbb::worker<DIM> > > HashMap;

    query_dispatcher( graph& g, const HashMap& h )
        : domain( h )
        , result()
        , node(g, 1, do_work(domain, result, node))
    {
    }

    struct do_work
    {
        do_work(const HashMap& d, function_node<par_tbb::query_data, msg_data> &n)
            : domain( d )
            , result(r)
            , node(n)
        {
        }

        continue_msg operator() (const par_tbb::query_data &msg) const
        {
            const std::array<double,DIM>& coord = msg.coord;
            const tessel<DIM>& center           = msg.center;
            const double& radius                = msg.radius;
            const double& sq_radius             = msg.sq_radius;
            const double inc = exp2(range);
            const long r = (long)(radius / inc) + 1;

            long x,y,z;
            for ( x = -r; x <= r; ++x )
            {
                if ( x == 0 || ( x > 0 ?
                                coord[0] + radius > (center.coor[0] + x) * inc :
                                coord[0] - radius < (center.coor[0] + x + 1) * inc ) )
                {
                    for ( y = -r; y <= r; ++y )
                    {
                        if ( y == 0 || ( y > 0 ?
                                        coord[1] + radius > (center.coor[1] + y) * inc :
                                        coord[1] - radius < (center.coor[1] + y + 1) * inc ) )
                        {
                            for ( z = -r; z <= r; ++z )
                            {
                                if ( z == 0 || ( z > 0 ?
                                                coord[2] + radius > (center.coor[2] + z) * inc :
                                                coord[2] - radius < (center.coor[2] + z + 1) * inc ) )
                                {
                                    tessel<DIM> aux;
                                    aux.coor[0] = center.coor[0] + x;
                                    aux.coor[1] = center.coor[1] + y;
                                    aux.coor[2] = center.coor[2] + z;
                                    LOG("Point [ %7.3f, %7.3f, %7.3f ]", coord[0], coord[1], coord[2]);
                                    LOG_OBJ(" radius: " << radius << " looking in cell: " << aux << std::endl);

                                    typename HashMap::const_iterator found = domain.find(aux);
                                    if ( found != domain.end() )
                                    {
                                        ++number_of_request;
                                        LOG_OBJ("multi_query: number of request = " << number_of_request << std::endl);
                                        par_tbb::msg_data<DIM> msg;
                                        msg.address = id;
                                        msg.point = coord;
                                        msg.sq_radius = sq_radius;
                                        found->second->node.try_put( msg );
                                    }
                                }
                            }
                        }
                    }
                }
            }
            LOG("Ended multi querying, point: [ %f, ... ] radius: %f\n", coord[0], radius);
            return continue_msg();
        }

        const HashMap& domain;
        function_node<par_tbb::query_data, continue_msg> &node;
    };

    const HashMap& domain;
    function_node<par_tbb::query_data, continue_msg> &node;
};

} // namespace par_tbb

#endif // _R2BT_QUERY_DISPATCHER_TBB_2354792387529375923
