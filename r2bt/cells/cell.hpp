/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_CELL_NEIGHBORHOOD_ASF43R5Q34
#define _R2BT_CELL_NEIGHBORHOOD_ASF43R5Q34

namespace r2bt {

template < unsigned DIM >
class cell {
public:
    virtual ~cell() { }

    virtual const std::string& name() = 0;

    virtual void set_limits(const std::array<double, DIM>& x0, double range) = 0;

    virtual void reserve( size_t ) = 0;

    /* All searching points to be added are inside the cell */
    virtual void add(const std::array<double, DIM>& searching, uintptr_t ptr) = 0;

    /* To be called after all point are added, default implementation does nothing */
    virtual void build() { }

    /* Queries for neighbor points to querying point in a ball of radius. */
    virtual query_result query(const std::array<double, DIM>& querying, double radius, double sq_radius) const = 0;

    virtual kquery_result kquery(const std::array<double, DIM>& querying, size_t number_of, double target_sq_radius = -1.0 ) = 0;

};

} // namespace

#endif // _R2BT_CELL_NEIGHBORHOOD_ASF43R5Q34
