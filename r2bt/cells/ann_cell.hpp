/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_ANN_CELL_NEIGHBORHOOD_ON3OIN3NF3N4ONONO202JOD
#define _R2BT_ANN_CELL_NEIGHBORHOOD_ON3OIN3NF3N4ONONO202JOD

#ifdef ANN_WAS_FOUND

#include <ANN/ANN.h>
#include "cell.hpp"

namespace r2bt
{

template < unsigned DIM >
class ann_cell : public cell<DIM>
{
public:
    ann_cell() :
        nPts ( 0 ),
        kdTree ( nullptr )
    {
        LOG_OBJ("Creating ann_cell\n");
        queryPt = annAllocPt(DIM); // allocate query point
        LOG_OBJ("\tCreating queryPt\n");
    }

    virtual ~ann_cell()
    {
        LOG_OBJ("Destroying ann_cell\n");
        annDeallocPt(queryPt); LOG_OBJ("\tDestroying query point\n");
        delete kdTree; LOG_OBJ("\tDestroying kd_tree\n");
        annClose(); LOG_OBJ("\tClosing ANN\n");
    }

    const std::string& name() override
    {
        static std::string the_name("Ann");
        return the_name;
    }

    void reserve(size_t _size) override
    {
        dataPts = annAllocPts(_size, DIM); // allocate data points
    }

    void set_limits(const std::array<double, DIM>& x0, double range) override
    {
        origin = x0;
        length = range;
    }

    void add(const std::array<double, DIM>& searching, uintptr_t ptr) override
    {
        for ( unsigned j = 0; j < DIM; ++j )
        {
            dataPts[nPts][j] = searching[j];
        }

        ref_vector.push_back( ptr );

        ++nPts;
    }

    static cell<DIM>* create()
    {
        LOG_OBJ("Cloning ann_cell\n");
        return new ann_cell();
    }

    void build() override
    {
        if ( kdTree != nullptr )
        {
            delete kdTree;
        }

        LOG_OBJ("\tCreating tree\n");
        kdTree = new ANNkd_tree( // build search structure
            dataPts, // the data points
            nPts, // number of points
            DIM); // dimension of space
    }

    query_result query(const std::array<double, DIM>& querying, double radius, double sq_radius) const override
    {
        query_result result;
        size_t len = ref_vector.size();
        result.reserve( len / 2); //Only grows once

        for ( unsigned j = 0; j < DIM; ++j )
        {
            queryPt[j] = querying[j];
        }
        int query_size = kdTree->annkFRSearch( //Fixed radius search
            queryPt, // query point
            sq_radius, // sq_radius
            0, // search for all the points within the radius
            nullptr, // nearest neighbors (returned)
            nullptr, // distance (returned)
            0.0 // error bound
        );

        ANNidxArray nnIdx; // near neighbor indexes
        ANNdistArray dists; // near neighbor distances
        nnIdx = new ANNidx[query_size];
        dists = new ANNdist[query_size];

        kdTree->annkFRSearch( //Fixed radius search
            queryPt, // query point
            sq_radius, // sq_radius
            query_size, // search for all the points within the radius
            nnIdx, // nearest neighbors (returned)
            dists, // distance (returned)
            0.0 // error bound
        );

        for( int j = 0; j < query_size; j++)
        {
            result.push_back ( ref_vector[ nnIdx[j] ] );
        }

        delete [] nnIdx; //LOG_OBJ("\Cleaning indexes\n");
        delete [] dists; //LOG_OBJ("\Cleaning distances\n");

        return std::move(result);
    }

    kquery_result kquery(const std::array<double, DIM>& querying, size_t K, double target_sq_radius = -1.0 ) override
    {
        kquery_result result;
        if ( K > ref_vector.size() )
        {
            for ( unsigned j = 0; j < DIM; ++j )
            {
                queryPt[j] = querying[j];
            }
            unsigned index;
            for( index= 0; index < ref_vector.size(); index++ )
            {
                double d = annDist(
                            DIM, // dimension of space
                            dataPts[index],
                            queryPt);
                norm_and_refs.emplace_front( d, ref_vector[index] );
            }
        }
        else
        {
            ANNidxArray nnIdx; // near neighbor indexes
            ANNdistArray dists; // near neighbor distances

            nnIdx = new ANNidx[K];
            dists = new ANNdist[K];

            for ( unsigned j = 0; j < DIM; ++j )
            {
                queryPt[j] = querying[j];
            }

            kdTree->annkSearch( // k search
                queryPt, // query point
                K, // number of near neighbors
                nnIdx, // nearest neighbors (returned)
                dists, // distance (returned)
                0.0 // error bound
            );

            for( unsigned j = 0; j < K; j++)
            {
                result.emplace_front( dists[j], ref_vector[ nnIdx[j] ]  );
            }
            delete [] nnIdx; //LOG_OBJ("\Cleaning indexes\n");
            delete [] dists; //LOG_OBJ("\Cleaning distances\n");
        }
        result.sort();
        return std::move( result );
    }

private:
    ANNpointArray dataPts; // data points
    int nPts; // actual number of data points
    ANNpoint queryPt; // query point
    ANNkd_tree* kdTree; // search structure

    internal_point<DIM> origin;
    double length = INFINITY ;

    typedef std::vector< uintptr_t > RefVector;
    RefVector ref_vector;

    kquery_result norm_and_refs;
};

} // namespace

#endif //ANN_WAS_FOUND

#endif //_R2BT_ANN_CELL_NEIGHBORHOOD_ON3OIN3NF3N4ONONO202JOD
