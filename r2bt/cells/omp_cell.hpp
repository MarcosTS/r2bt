/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_OMP_CELL_3584593845QW098523
#define _R2BT_OMP_CELL_3584593845QW098523

#ifdef OPENMP_WAS_FOUND

#include "base_cell.hpp"

namespace r2bt
{

template < unsigned DIM >
class omp_cell : public base_cell<DIM>
{
public:
    virtual ~omp_cell()
    {
        LOG_OBJ("Destroying omp_cell\n");
    }

    const std::string& name() override
    {
        static std::string the_name("omp");
        return the_name;
    }

    static cell<DIM>* create()
    {
        LOG_OBJ("Cloning omp_cell\n");
        return new omp_cell();
    }

    query_result query(const std::array<double, DIM>& querying, double radius, double sq_radius) const override
    {
        query_result result;
        size_t i = 0, len = this->point_vector.size();
        result.reserve( len / 2); //Only grows once

        #pragma omp parallel
        {
            query_result private_result;
            private_result.reserve(len / 2); //Only grows once

            #pragma omp for
            for ( i = 0; i < len; ++i )
            {
                const auto& p = this->point_vector[i];
                if ( p.norm( querying ) < sq_radius )
                {
                    private_result.push_back( this->ref_vector[i] );
                }
            }
            #pragma omp critical
            result.insert(result.end(), private_result.begin(), private_result.end());
        }

        return std::move(result);
    }

    /* K Nearest Neighbour search */
    kquery_result kquery(const std::array<double, DIM>& querying, size_t K, double target_sq_radius = -1.0 ) override
    {
        kquery_result result;
        size_t len = this->point_vector.size();

        #pragma omp parallel
        {
            kquery_result private_result;

            #pragma omp for
            for ( size_t i = 0; i < len; ++i )
            {
                double sq_distance = this->point_vector[i].norm( querying );
                if ( target_sq_radius < 0 || sq_distance < target_sq_radius )
                {
                    private_result.emplace_front( sq_distance, this->ref_vector[i] );
                }
            }
            #pragma omp critical
            result.merge(private_result);
        }
        result.resize( K );
        return std::move(result);
    }
};

} // namespace


#endif // OPENMP_WAS_FOUND

#endif // _R2BT_OMP_CELL_3584593845QW098523
