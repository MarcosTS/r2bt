/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_INTERNAL_POINT_QJRWOJAOFJAWFAJLGAAFAAHAHSTU3247
#define _R2BT_INTERNAL_POINT_QJRWOJAOFJAWFAJLGAAFAAHAHSTU3247

namespace r2bt {

template < unsigned DIM >
class internal_point : public std::array<double, DIM>
{
public:
    internal_point () {}

    internal_point ( const std::array<double, DIM>& p )
    {
        for(unsigned i = 0; i < DIM; ++i)
        {
            (*this)[i] = p[i];
        }
    }

    inline double norm( const internal_point& p2 ) const
    {
        double sum = 0; unsigned i;
        for ( i = 0; i < DIM; ++i ) {
            sum += ((*this)[i]-p2[i])*((*this)[i]-p2[i]);
        }
        return sum;
    }

    inline double norm( const std::array<double, DIM>& p2 ) const
    {
        double sum = 0; unsigned i;
        for ( i = 0; i < DIM; ++i ) {
            sum += ((*this)[i]-p2[i])*((*this)[i]-p2[i]);
        }
        return sum;
    }

    #ifdef R2BT_VERBOSE
    void print_point()
    {
        unsigned i;
        LOG_OBJ(" [ ");
        for( i = 0; i < DIM; ++i)
        {
            LOG_OBJ( (*this)[i] << " ");
        }
        LOG_OBJ("]" << std::endl);
        return;
    }
    #endif // R2BT_VERBOSE
};

template < unsigned DIM >
class tree_point : public std::array<double, DIM>
{
public:
    tree_point () {}

    tree_point ( const std::array<double, DIM>& p )
    {
        for(unsigned i = 0; i < DIM; ++i)
        {
            (*this)[i] = p[i];
        }
    }

    inline double norm( const tree_point& p2 ) const
    {
        double sum = 0; unsigned i;
        for ( i = 0; i < DIM; ++i ) {
            sum += ((*this)[i]-p2[i])*((*this)[i]-p2[i]);
        }
        return sum;
    }

    inline double norm( const std::array<double, DIM>& p2 ) const
    {
        double sum = 0; unsigned i;
        for ( i = 0; i < DIM; ++i ) {
            sum += ((*this)[i]-p2[i])*((*this)[i]-p2[i]);
        }
        return sum;
    }

    /* Indexes of the childs, leafs are denoted with -1 */
    int right = -1;
    int left = -1;
};

} // namespace

#endif // _R2BT_INTERNAL_POINT_QJRWOJAOFJAWFAJLGAAFAAHAHSTU3247
