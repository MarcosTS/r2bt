/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_ITREE_CELL_NEIGHBORHOOD_ONF2OR2ONFO2NONQFNOQN
#define _R2BT_ITREE_CELL_NEIGHBORHOOD_ONF2OR2ONFO2NONQFNOQN

#include "base_cell.hpp"

/**
    Implicit tree
    Warning This tree is not finished!!
**/

namespace r2bt {

template < unsigned DIM >
class itree_cell : public base_cell<DIM>
{
public:
    virtual ~itree_cell()
    {
        LOG_OBJ("Destroying itree_cell\n");
    }

    const std::string& name() override
    {
        static std::string the_name("itree");
        return the_name;
    }

    cell<DIM>* clone() const override
    {
        LOG_OBJ("Cloning itree_cell\n");
        return new itree_cell;
    }

     /* Point to look neighbours, could be inside or outside the cell */
    query_result query(const std::array<double, DIM>& querying, double radius, double sq_radius) override
    {
        // Clear previous results
        this->result.clear();
        size_t _size = this->point_vector.size();
        this->result.reserve( _size / 2 ); // Only grows once
        query_recursive( rootPlane, querying, sq_radius, this->origin );
        return this->result;
    }

    /* K Nearest Neighbour search */
    kquery_result kquery(const std::array<double, DIM>& querying, size_t K, double target_sq_radius = -1.0 ) override
    {

        kquery_result result;
        /*
        std::vector < unsigned > idx;
        unsigned max_idx = 0;
        double max_dist = 0;
        double x_max_dist = 0;
        kquery_recursive ( rootPlane, querying, idx, K, origin, &max_idx, &max_dist, &x_max_dist );
        result.reserve( K );

        int i;
        for( i = 0; i < idx.size(); ++i)
        {
            result.push_back( ref_vector[idx[i]]);
        }

        return { result.data(), result.size() };
    */
        throw std::runtime_error("kquery query not implemented in itree_cell...");
        return std::move( result );
    }

    void build() override
    {
        rootPlane = std::make_shared<Plane>();
        build_recursive ( 0, this->point_vector.size()-1, rootPlane, this->origin );
    }

#ifdef R2BT_VERBOSE
    void print_tree();
    void check_tree();
#endif // R2BT_VERBOSE

private:

    struct Plane
    {
        int index = -1;
        std::shared_ptr<Plane> left;
        std::shared_ptr<Plane> right;
        /*
        ~Plane()
        {
            LOG_OBJ("Destroying itree_cell\n");
        }
        */
    };

    typedef std::shared_ptr<Plane> ptrPlane;
    ptrPlane rootPlane;

    inline void swap( unsigned idx1, unsigned idx2 )
    {
        this->point_vector[idx1].swap( this->point_vector[idx2] );

        uintptr_t tmpVoid;
        tmpVoid = this->ref_vector[idx1];
        this->ref_vector[idx1] = this->ref_vector[idx2];
        this->ref_vector[idx2] = tmpVoid;
    }


    /* See https://en.wikipedia.org/wiki/Dutch_national_flag_problem */
    int partition (int start, int end, const double splitting_value, const unsigned axis)
    {
        int left = start;
        int mid = start;
        int right = end;
        while ( mid <= right )
        {
            if( this->point_vector[mid][axis] < splitting_value )
            {
                if( left != mid ) swap(left, mid);
                ++left;
                ++mid;
            }
            else if ( this->point_vector[mid][axis] > splitting_value )
            {
                if( mid != right ) swap(mid, right);
                --right;
            }
            else ++mid;
        }
        if( right < start ) return start;
        else return right;
    }

    void build_recursive(int start, int end, ptrPlane plane, std::array<double, DIM> x0, unsigned depth = 1, unsigned axis = 0)
    {
        if( start >= end )
        {
            plane->index = end;
            (this->point_vector[end]).print_point();
            return;
        }

        if( axis >= DIM)
        {
            axis = 0;
            depth ++;
        }

        double splitting_value = x0[axis] + ( this->length / pow(2, depth) );
        int split_index = partition( start, end, splitting_value, axis );
        LOG_OBJ("\nStart: " << start << ", Split_index: " << split_index << ", End: " << end << std::endl);
        LOG_OBJ("Range: " << ( this->length / pow(2, depth) ) << ", Splitting_value: " << splitting_value << ", Axis: " << axis << std::endl);

        plane->left = std::make_shared<Plane>();
        LOG_OBJ("Left");
        build_recursive( start, split_index, plane->left, x0, depth, axis + 1);

        x0[axis] = splitting_value;

        plane->right = std::make_shared<Plane>();
        LOG_OBJ("Right");
        build_recursive( split_index + 1, end, plane->right, x0, depth, axis + 1);
    }

    void query_recursive(ptrPlane plane, const std::array<double, DIM>& querying, const double sq_radius, std::array<double,DIM> x0, unsigned depth = 1, unsigned axis = 0)
    {
        if(plane == NULL) return;
        int index = plane->index;

        if( index == -1)
        {
            if( axis >= DIM)
            {
                axis = 0;
                depth ++;
            }

            double splitting_value = x0[axis] + ( this->length / pow(2, depth) );
            double dx, dx2;
            dx = splitting_value - querying[axis];
            dx2 = dx*dx;

            LOG_OBJ("Index: " << index << ", Splitting_value: " << splitting_value << ", Range: " << ( this->length / pow(2, depth) ) << ", Axis: " << axis << std::endl);

            x0[axis] = ( dx >= 0 )? x0[axis] : splitting_value;
            query_recursive( dx >= 0 ? plane->left : plane->right, querying, sq_radius, x0, depth, axis + 1);
            if ( dx2 > sq_radius ) return;
            x0[axis] = ( dx >= 0 )? splitting_value : x0[axis];
            query_recursive( dx >= 0 ? plane->right : plane->left, querying, sq_radius, x0, depth, axis + 1);

        }
        else
        {
            //std::cout << ((Point<DIM>*)ref_vector[index])->label <<"\n";
            if( this->point_vector[index].norm( querying ) < sq_radius)
            {
                this->result.push_back( this->ref_vector[index] );
            }
        }
    }

    void kquery_recursive(ptrPlane plane, const double querying[DIM], query_result& neighbors, const int K, std::array<double,DIM>x0,
                            unsigned *max_idx, double *max_dist, double *x_max_dist, unsigned depth = 1, unsigned axis = 0)
    {
        /*
        if(plane == NULL) return;
        int index = plane->index;
        unsigned stored = neighbors.size();

        if( index == -1)
        {
            if( axis >= DIM) {
            axis = 0;
            depth ++;
            }
            double range, splitting_value, dx, dx2;
            range = cell_range / (double) int_pow(2, depth);
            splitting_value = x0[axis] + range;
            dx = splitting_value - querying[axis];
            dx2 = dx*dx;
            //std::cout << "Index: " << index << ", Splitting_value: " << splitting_value << ", Range: " << range << ", Axis: " << axis << std::endl;
            x0[axis] = ( dx >= 0 )? x0[axis] : splitting_value;
            kquery_recursive( dx >= 0 ? plane->left : plane->right, querying, neighbors, K, x0, max_idx, max_dist, x_max_dist, depth, axis + 1);
            if ( dx2 > *max_dist && stored == K) return;
            x0[axis] = ( dx >= 0 )? splitting_value : x0[axis];
            kquery_recursive( dx >= 0 ? plane->right : plane->left, querying, neighbors, K, x0, max_idx, max_dist, x_max_dist, depth, axis + 1);
        }
        else
        {
            double d = norm(point_vector[index].p, querying);
            //std::cout << ((Point<DIM>*)ref_vector[index])->label <<"\n";
            if( stored < K )
            {
                neighbors.push_back( index );
                if( d > *max_dist ){
                    *max_dist = *x_max_dist = d;
                    *max_idx = stored;
                }
                stored++;
            }
            else if( d == *max_dist )
            {
                neighbors.push_back( index );
                *x_max_dist = d;
            }
            else if ( d < *max_dist )
            {
                if( *max_dist == *x_max_dist ) neighbors.push_back( neighbors[*max_idx] );
                neighbors[*max_idx] = index;
                *max_dist = 0;
                int i;
                for( i = 0; i < K; ++i )
                {   // Tag the biggest distance
                    double aux_dist = norm(point_vector[neighbors[i]], querying);
                    if( aux_dist > *max_dist){
                        *max_dist = aux_dist;
                        *max_idx = i;
                    }
                }
                if( *max_dist < *x_max_dist )
                {
                    neighbors.resize ( K );
                    *x_max_dist = *max_dist;
                }
            }
        }
        */
    }


#ifdef R2BT_VERBOSE
    void print_tree_recursive(const ptrPlane plane, std::array<double, DIM> x0, unsigned depth = 1, unsigned axis = 0 );
    void check_tree_recursive(ptrPlane plane, std::array<double, DIM> x0, unsigned depth = 1, unsigned axis = 0 );
#endif // R2BT_VERBOSE

};

#ifdef R2BT_VERBOSE

template < unsigned DIM >
void itree_cell<DIM>::print_tree ()
{
    print_tree_recursive( rootPlane, this->origin );
}

template < unsigned DIM >
void itree_cell<DIM>::check_tree()
{
    check_tree_recursive( rootPlane, this->origin );
}

template < unsigned DIM >
void itree_cell<DIM>::print_tree_recursive(const ptrPlane plane, std::array<double, DIM> x0, unsigned depth, unsigned axis )
{
    if( axis >= DIM) {
        axis = 0;
        depth++;
    }

    double splitting_value = x0[axis] + ( this->length / pow(2, depth) );
    int index = plane->index;

    if( index != -1 )
    {
        (this->point_vector[index]).print_point();
        return;
    }
    std::cout << std::endl;

    if(plane->left)
    {
        for ( unsigned i = 0; i < (depth + axis); i++ )
        {
            std::cout << "\t";
        }
        std::cout << "Left partition: [ ";
        for( unsigned i = 0; i < DIM; ++i)
        {
            std::cout << x0[i] << " ";
        }
        std::cout << "], axis: " << axis ;
        print_tree_recursive( plane->left, x0, depth, axis + 1 );

    }

    if(plane->right)
    {
        x0[axis] = splitting_value;
        for ( unsigned i = 0; i < (depth + axis); i++ )
        {
            std::cout << "\t";
        }
        std::cout << "Right partition: [ ";
        for( unsigned i = 0; i < DIM; ++i)
        {
            std::cout << x0[i] << " ";
        }
        std::cout << "], axis: " << axis ;
        print_tree_recursive( plane->right, x0, depth, axis + 1 );
    }
}

template < unsigned DIM >
void itree_cell<DIM>::check_tree_recursive(ptrPlane plane, std::array<double, DIM> x0, unsigned depth, unsigned axis )
{
    if( axis >= DIM)
    {
        axis = 0;
        depth++;
    }

    double range = ( this->length / pow(2, depth) );
    double splitting_value = x0[axis] + range;

    if(plane->left)
    {
        check_tree_recursive(plane->left, x0, depth, axis + 1);
    }
    if(plane->right)
    {
        x0[axis] = splitting_value;
        check_tree_recursive(plane->right, x0, depth, axis + 1);
    }

    int index = plane->index;
    if ( index != -1 )
    {
        if( this->point_vector[index][axis] <= (splitting_value - range) )
        {
            std::cout << "For axis " << axis << ", point:\n";
            (this->point_vector[index]).print_point();
            std::cout << "is smaller than the splitting_value - range: " << splitting_value - range << std::endl;
        }

        if( this->point_vector[index][axis] > (splitting_value + range) )
        {
            std::cout << "For axis " << axis << ", point:\n";
            (this->point_vector[index]).print_point();
            std::cout << "is bigger than the splitting_value + range: " << splitting_value + range << std::endl;
        }
    }
}
#endif // R2BT_VERBOSE

}

#endif // _R2BT_ITREE_CELL_NEIGHBORHOOD_ONF2OR2ONFO2NONQFNOQN
