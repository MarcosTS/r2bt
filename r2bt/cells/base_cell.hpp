/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_BASE_CELL_0Q934FJAFAW4J0
#define _R2BT_BASE_CELL_0Q934FJAFAW4J0

#include "internal_points.hpp"
#include "cell.hpp"

namespace r2bt {

template < unsigned DIM >
class base_cell : public cell<DIM>
{
public:
    virtual ~base_cell() { }

    void set_limits(const std::array<double, DIM>& x0, double range) override
    {
        origin = x0;
        length = range;
    }

    void reserve( size_t _size) override
    {
        point_vector .reserve( _size );
        ref_vector   .reserve( _size );
    }

    void add(const std::array<double, DIM>& searching, uintptr_t ptr) override
    {
        internal_point<DIM> s( searching );
        point_vector.push_back( s );
        ref_vector  .push_back( ptr );
    }

    typedef internal_point<DIM> PointDim;

    typedef std::vector< PointDim > PointVector;
    typedef std::vector< uintptr_t > RefVector;

    PointVector point_vector;
    RefVector ref_vector;

    PointDim origin;
    double length = INFINITY ;
};

} // namespace

#endif // _R2BT_CELL_NEIGHBORHOOD_ASF43R5Q34
