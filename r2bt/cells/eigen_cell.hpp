/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef R2BT_EIGEN_CELL_NEIGHBORHOOD_235ASGAGSDAVAHJDA
#define R2BT_EIGEN_CELL_NEIGHBORHOOD_235ASGAGSDAVAHJDA

//#define EIGEN3_WAS_FOUND

#ifdef EIGEN3_WAS_FOUND

// Note: Accidentally I removed eigen_cell when typing git commands.
// Nevermind, because it is useless, it is worse than brute-force plain-C++ solution

#include "cell.hpp"

template < unsigned DIM >
class eigen_cell : public base_cell<DIM>
{
public:
    virtual ~eigen_cell()
    {
         LOG_OBJ("Destroying eigen_cell\n");
    }

    cell<DIM>* clone() const override
    {
        LOG_OBJ("Cloning eigen_cell\n");
        return new eigen_cell;
    }

    void reserve( size_t _size ) override
    {
        //ref_vector.reserve( _size );
    }

    void add(const std::array<double, DIM>& searching, uintptr_t ptr) override
    {
    /*
        unsigned i;
        for ( i = 0; i < DIM; ++i ) {
            point_vector    .push_back( searching[i] );
            point_aux_vector.push_back( searching[i] );
        }
        this->ref_vector  .push_back( ptr );
    */
    }

    /* Point to look neighbours, could be inside or outside the cell */
    query_result query(const std::array<double, DIM>& querying, double sq_radius) override
    {
        return query_result();
    }


    /* K Nearest Neighbour search */
    kquery_result kquery(const std::array<double, DIM>& querying, size_t K, double target_sq_radius = -1.0 ) override
    {
        throw std::runtime_error("kquery query not implemented in par_cell...");
        kquery_result result;
        return result;
    }
    /*
    typedef Matrix<double, DIM, 1> PointVector;
    typedef Matrix<double, DIM, Dynamic> PointMatrix;
    typedef std::vector< uintptr_t > RefVector;

    RefVector ref_vector;
    PointMatrix point_vector;
    */
};

#endif // EIGEN3_WAS_FOUND

#endif // R2BT_EIGEN_CELL_NEIGHBORHOOD_235ASGAGSDAVAHJDA
