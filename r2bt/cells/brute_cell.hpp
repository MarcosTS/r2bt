/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/
#ifndef _R2BT_BRUTE_CELL_NEIGHBORHOOD_Q03U5582058W3
#define _R2BT_BRUTE_CELL_NEIGHBORHOOD_Q03U5582058W3

#include "base_cell.hpp"

namespace r2bt {

template < unsigned DIM >
class brute_cell : public base_cell<DIM>
{
public:
    virtual ~brute_cell()
    {
        LOG_OBJ("Destroying brute_cell\n");
    }

    const std::string& name() override
    {
        static std::string the_name("brute-force");
        return the_name;
    }

    static cell<DIM>* create()
    {
        LOG_OBJ("Create brute_cell\n");
        return new brute_cell();
    }

    /* Point to look neighbours, could be inside or outside the cell */
    query_result query(const std::array<double, DIM>& querying, double radius, double sq_radius) const override
    {
        query_result result;
        size_t i = 0, len = this->point_vector.size();
        result.reserve( len / 2); //Only grows once

        for ( ; i < len; ++i )
        {
            const auto& p = this->point_vector[i];
            if ( p.norm( querying ) < sq_radius )
            {
                result.push_back( this->ref_vector[i] );
            }
        }

        return std::move(result);
    }

    /* K Nearest Neighbour search */
    kquery_result kquery(const std::array<double, DIM>& querying, size_t K, double target_sq_radius = -1.0 ) override
    {
        kquery_result result;
        size_t i = 0;
        for ( const auto& p : this->point_vector )
        {
            double sq_distance = p.norm( querying );
            if ( target_sq_radius < 0 || sq_distance < target_sq_radius )
            {
                result.emplace_front( sq_distance, this->ref_vector[i] );
            }
            ++i;
        }
        result.sort();
        result.resize( K );
        return std::move(result);
    }

};

} // namespace

#endif // _R2BT_BRUTE_CELL_NEIGHBORHOOD_Q03U5582058W3
