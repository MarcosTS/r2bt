/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#ifndef INCLUDE_DOCS_HPP_5327592837592835
#define INCLUDE_DOCS_HPP_5327592837592835

/*! \brief namespace for r2bt classes and global functions
 *
 * Including searching sets, insert methods, query methods and others.
 */
namespace r2bt {

/*! \brief namespace for factory global functions
 *
 * This nested namespace contains some functions that return searching set using
 * a factory class.
 */
namespace factory {

/*! \brief Creates a searching set object
 *
 * It creates a object from one of the available alternatives and returns a pointer to it.
 * Client code holds the ownership of the new object.
 * @param name class name of the required searching set, possible values are: "Serial", "SerialRec",
 * "TBB", "OneCell", "OMP". Some of them might not be available when the dependencies are not available.
 * @param h cell size recommendation, it will be transformed in a value that is a power of 2.
 * @param cell_name class name of the inner cells to be used. Available options are: "brute",
 * "stree", "utree", "ann", "omp". Best choice in a general case is "stree".
 * @return the newly created searching set.
 */
template <unsigned DIM, typename Point >
searching_set<DIM>* createRawPointer(const std::string& name, double h, const std::string& cell_name);

/*! \brief Creates a searching set object
 *
 * It creates a object from one of the available alternatives and returns a shared_ptr holding the address
 * of the new object. For further details see: createRawPointer.
 */
template <unsigned DIM, typename Point >
std::shared_ptr< searching_set<DIM> > createShared(const std::string& name, double h, const std::string& cell_name);

/*! \brief Creates a searching set object
 *
 * It creates a object from one of the available alternatives and returns a unique_ptr holding the address
 * of the new object. For further details see: createRawPointer.
 */
template <unsigned DIM, typename Point >
std::unique_ptr< searching_set<DIM> > createUniquePtr(const std::string& name, double h, const std::string& cell_name);

} //namespace factory

/*! \brief Inserts points in a searching set
 *
 * It inserts points from iterator [ first, last ) into the searching set. Points are converted to coordinates
 * (std::array<double, DIM>) by the functor. If the iterator has random_access tag an index will be stored along
 * with the point for future reference. If not the address of the point (a raw pointer) will be used for reference.
 * Iterator could point directly to the point or to a pointer to the point, i.e, it is possible to use an
 * iterator from a list of pointer to points.
 *
 * @param neigh the searching set object to insert the points into. Usually one of the object returned
 * by one of the functions to create searching sets.
 * @param first an iterator pointing to the first point to be inserted.
 * @param last an iterator referring to the past-the-end point to be inserted.
 * @param point2coordinates a functor that converts a point from client code to its coordinates,
 * its signature should be equivalent to:
 * @code{.cpp}
 *  std::array<double, 3> point2coordinates(const a_point& p)
 * or
 *  std::array<double, 3> point2coordinates(const a_point* p)
 * @endcode
 */
template<typename Iterator, typename Neighborhood, typename Functor>
void insert(Neighborhood& neigh, Iterator first, Iterator last, Functor point2coordinates);

/*! \brief Overloads insert function for pointer to searching sets
 *
 */
template<typename Iterator, typename Neighborhood, typename Functor>
void insert(Neighborhood* neigh, Iterator first, Iterator last, Functor point2coordinates);

/*! \brief queries for neighbors of the given points in the given searching set
 *
 * Given a searching set (already created and populated with points), this function looks for neighbors
 * of the points given by the iterators among the points in the searching set. It returns an associative
 * container, its keys are a reference to the querying point and the values are a std vector of references
 * to the points in the searching set. The references for the querying points are indexes if the iterators
 * have the random access tag and a pointer otherwise. The reference for the searching points are indexes
 * if the searching points were inserted using a random access iterator and a pointer otherwise.
 *
 * @param neigh a pointer to a searching set
 * @param first an iterator pointing to the first point to query.
 * @param last an iterator referring to the past-the-end point to query.
 * @param size_ number of querying points
 * @param point2coordinates a functor that converts a querying point from client code to its coordinates,
 * its signature should be equivalent to:
 * @code{.cpp}
 *  std::array<double, 3> point2coordinates(const a_point& p)
 * or
 *  std::array<double, 3> point2coordinates(const a_point* p)
 * @endcode
 * @param radius the radius of the ball in which neighbors are found.
 * @returns a associative container with Keys: reference to querying points and Values: vector of references to
 * searching points.
 */
template<typename Iterator, typename Neighborhood, typename FunctorCoordinates>
multiquery_result_t query(Neighborhood* neigh,
                          Iterator first, Iterator last, size_t size_, // Iterators and size over querying points
                          FunctorCoordinates point2coordinates, // Convert a querying point to std::array
                          double radius);

/*! \brief queries for neighbors of the given points in the given searching set
 *
 * Given a searching set (already created and populated with points), this function looks for neighbors
 * of the points given by the iterators among the points in the searching set. It returns an associative
 * container, its keys are a reference to the querying point and the values are a std vector of references
 * to the points in the searching set. The references for the querying points are indexes if the iterators
 * have the random access tag and a pointer otherwise. The reference for the searching points are indexes
 * if the searching points were inserted using a random access iterator and a pointer otherwise.
 *
 * @param neigh a reference to a searching set
 * @param first an iterator pointing to the first point to query.
 * @param last an iterator referring to the past-the-end point to query.
 * @param size_ number of querying points
 * @param point2coordinates a functor that converts a querying point from client code to its coordinates,
 * its signature should be equivalent to:
 * @code{.cpp}
 *  std::array<double, 3> point2coordinates(const a_point& p)
 * or
 *  std::array<double, 3> point2coordinates(const a_point* p)
 * @endcode
 * @param radius the radius of the ball in which neighbors are found.
 * @returns a associative container with Keys: reference to querying points and Values: vector of references to
 * searching points.
 */
template<typename Iterator, typename Neighborhood, typename FunctorCoordinates>
multiquery_result_t query(Neighborhood& neigh,
                          Iterator first, Iterator last, size_t size_,
                          FunctorCoordinates point2coordinates, // Convert a querying point to std::array
                          double radius);

/*! \brief queries for neighbors of the given points in the given searching set
 *
 * Given a searching set (already created and populated with points), this function looks for neighbors
 * of the points given by the iterators among the points in the searching set. It uses the corresponding
 * radius in the given radius iterators. One radius for each querying point. It returns an associative
 * container, its keys are a reference to the querying point and the values are a std vector of references
 * to the points in the searching set. The references for the querying points are indexes if the iterators
 * have the random access tag and a pointer otherwise. The reference for the searching points are indexes
 * if the searching points were inserted using a random access iterator and a pointer otherwise. A different
 * query radius is provided for each querying point by  iterators.
 *
 * @param neigh a reference to a searching set
 * @param first an iterator pointing to the first point to query.
 * @param last an iterator referring to the past-the-end point to query.
 * @param size_ number of querying points
 * @param point2coordinates a functor that converts a querying point from client code to its coordinates,
 * its signature should be equivalent to:
 * @code{.cpp}
 *  std::array<double, 3> point2coordinates(const a_point& p)
 * or
 *  std::array<double, 3> point2coordinates(const a_point* p)
 * @endcode
 * @param first_radius an iterator pointing to the first radius (a double).
 * @param last_radius an iterator referring to the past-the-end radius.
 *
 * @returns a associative container with Keys: reference to querying points and Values: vector of references to
 * searching points.
 */
template<typename Iterator, typename Neighborhood, typename RadiusIterator, typename FunctorCoordinates>
multiquery_result_t query(Neighborhood& neigh,
                          Iterator first, Iterator last, size_t size_,
                          FunctorCoordinates point2coordinates,
                          RadiusIterator first_radius, RadiusIterator last_radius);

/*! \brief queries for neighbors of the given points in the given searching set
 *
 * Given a searching set (already created and populated with points), this function looks for neighbors
 * of the points given by the iterators among the points in the searching set. It uses the corresponding
 * radius in the given radius iterators. One radius for each querying point. It returns an associative
 * container, its keys are a reference to the querying point and the values are a std vector of references
 * to the points in the searching set. The references for the querying points are indexes if the iterators
 * have the random access tag and a pointer otherwise. The reference for the searching points are indexes
 * if the searching points were inserted using a random access iterator and a pointer otherwise. A different
 * query radius is provided for each querying point by  iterators.
 *
 * @param neigh a pointer to a searching set
 * @param first an iterator pointing to the first point to query.
 * @param last an iterator referring to the past-the-end point to query.
 * @param size_ number of querying points
 * @param point2coordinates a functor that converts a querying point from client code to its coordinates,
 * its signature should be equivalent to:
 * @code{.cpp}
 *  std::array<double, 3> point2coordinates(const a_point& p)
 * or
 *  std::array<double, 3> point2coordinates(const a_point* p)
 * @endcode
 * @param first_radius an iterator pointing to the first radius (a double).
 * @param last_radius an iterator referring to the past-the-end radius.
 *
 * @returns a associative container with Keys: reference to querying points and Values: vector of references to
 * searching points.
 */
template<typename Iterator, typename Neighborhood, typename RadiusIterator, typename FunctorCoordinates>
multiquery_result_t query(Neighborhood* neigh,
                          Iterator first, Iterator last, size_t size_,
                          FunctorCoordinates point2coordinates,
                          RadiusIterator first_radius, RadiusIterator last_radius);


} //namespace

#endif //INCLUDE_DOCS_HPP_5327592837592835
