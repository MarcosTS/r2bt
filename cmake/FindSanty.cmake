# This module finds Santy
# !!!!!!!!!!!!!! This is a dummy, not a real implmentaion !!!!!!!!!!!!!!!!!
#
# It sets the following variables:
#  Santy_FOUND              - Set to ___false____, or undefined
#  Santy_INCLUDE_DIR        - include directory.
#  Santy_LIBRARIES          - library files

# Santy is temporaly off
set ( Santy_FOUND OFF CACHE BOOL "Enable benchmarking with Santy" )
set ( Santy_INCLUDE_DIR "${PROJECT_SOURCE_DIR}/../santy" CACHE PATH "Path to Santy library.")
set ( Santy_LIBRARIES   "santy" CACHE PATH "Path to Santy library.")