
#############################################
#
#  This file tries to find all dependencies
#
#############################################

find_package(ANN)
if(ANN_FOUND)
    include_directories(${ANN_INCLUDE_DIR})
    add_definitions(-DANN_WAS_FOUND)
else()
    message(STATUS "ANN was not found")
endif()

find_package(TBB)
if(TBB_FOUND)
    include_directories(${TBB_INCLUDE_DIR})
    add_definitions(-DTBB_WAS_FOUND)
else()
    message("STATUS TBB was not found")
endif()

# TODO ZMQ find package is a dummy
find_package(ZMQ)
if(ZMQ_FOUND)
    include_directories(${ZMQ_INCLUDE_DIR})
    add_definitions(-DZMQ_WAS_FOUND)
else()
    message(STATUS "ZMQ was not found")
endif()

# TODO Santy find package is a dummy
#find_package(Santy)
#if(Santy_FOUND)
#    include_directories(${Santy_INCLUDE_DIR})
#    add_definitions(-DSanty_WAS_FOUND)
#else()
#    message(STATUS "Santy was not found")
#endif()

find_package(OpenMP)
if ( OPENMP_FOUND )
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    message(STATUS "Support for OpenMP found")
    add_definitions(-DOPENMP_WAS_FOUND)
else()
    message(STATUS "OpenMP support was not found")
endif()

function(target_link_optional_libs THE_TARGET)
    if(ANN_FOUND)
        target_link_libraries(${THE_TARGET} ${ANN_LIBRARIES})
    endif()
    if(TBB_FOUND)
        target_link_libraries(${THE_TARGET} ${TBB_LIBRARIES})
    endif()
    if(ZMQ_FOUND_FOUND)
        target_link_libraries(${THE_TARGET} ${ZMQ_LIBRARIES})
    endif()
    if(OPENMP_FOUND)
        target_link_libraries(${THE_TARGET} ${OPENMP_LIBRARIES})
    endif()
#    if( _FOUND)
#        target_link_libraries(${THE_TARGET} ${ _LIBRARIES})
#    endif()
endfunction()

