/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include <iostream>
#include <stdio.h>

#define R2BT_VERBOSE

#include <r2bt/r2bt.hpp>

using namespace r2bt;

int main(int argc, char** argv)
{
/*
    if ( argc != 3 ) {
        std::cerr << "usage: " << argv[0] << "range num" << std::endl;
        std::cerr << "being range the size of the cell (in 2^range) and num the number to locate" << std::endl;
        return 1;
    }
*/
    int range = 2; double radius = 4.11; // 0.3

    std::array<double, 3> coord = { 1.0, 4.1, 8.2 };

    std::cout << " Filtering cells using point = [ " << coord[0] << " " << coord[1] << " " << coord[2] << " ] " << std::endl;
    std::cout << "                       range = " << range << " and radius = " << radius << std::endl;

    for ( int r = 0; r <= 3; ++ r )
    {
        int i = 1;

        std::cout << std::endl << "++++++++++++ r = " << r << "   ++++++++++++" << std::endl << std::endl;
        const std::forward_list < std::array<int, 3> > &inc_list = multi_cell<3>::near_cells_3(r);

        multi_cell<3> mc(r);
        tessel<3> center = tessel<3>::get_tessel(coord, range);

        for ( auto &a : inc_list )
        {
            printf("%4i: ", i); ++i;
            for ( int x : a )
            {
                printf(" %4i ", x);
            }

            std::cout << "\t" << ( mc.filter( coord, radius, center, a ) ? "TRUE" : "false" );

            std::cout << std::endl;
        }
    }

    std::cout << std::endl << "++++++++++++ Cell Selects  ++++++++++++" << std::endl << std::endl;

    multi_cell<3> multi(range);

    std::forward_list< tessel<3> > sel = multi.select( coord, radius );

    for ( auto &t : sel )
    {
        std::cout << t << std::endl;
    }

    return 0;
}
