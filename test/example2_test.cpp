/****************************************************************************
 * (c) 2017 Copyright Santiago Tapia-Fernández (UPM)
 *                    Pablo Hiroshi Alonso-Miyazaki (UPM)
 *
 * This file is part of R2BT Library.
 *
 *     R2BT Library is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     R2BT Library is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with R2BT Library.  If not, see <http://www.gnu.org/licenses/>.
*****************************************************************************/

#include <iostream>

#include <r2bt/r2bt.hpp>
#include <r2bt/utils/test_point.hpp>

using namespace r2bt;

int main()
{
    double h = 8.0; // the cells will be sized 2 raised to 3

    auto neigh = factory::createUniquePtr<3, test_point>("serial_rec", h, "brute");

    test_point searching[] =
    {
        { {  2.0, 2.2, 4.3 }, 3 },
        { { 11.0, 2.2, 4.3 }, 4 },
        { {  8.1, 2.2, 4.3 }, 5 },
        { {  7.9, 2.2, 4.3 }, 6 },
    };
    size_t len = sizeof( searching ) / sizeof(test_point);

    std::cout << "Inserting " << len << " points" << std::endl;

    insert( *neigh, searching, searching+len, test_point2coordinates );

    if ( 1 ) // Individual querying
    {
        test_point q = { { 2.1, 2.2, 4.3 }, 1 };
        auto v = neigh->query(test_point2coordinates(q), 0.5, 0.5*0.5);

        std::cout << std::endl << "Neighbors of : " << q << std::endl;
        for ( auto index : v ) {
            std::cout << index << "\t"; // << searching[index] << std::endl;
        }
    }

    if ( 1 ) // Individual querying
    {
        test_point q = { { 8.01, 2.2, 4.3 }, 1 };
        auto v = neigh->query(test_point2coordinates(q), 0.3, 0.3*0.3);

        std::cout << std::endl << "Neighbors of : " << q << std::endl;
        for ( auto index : v ) {
            std::cout << index << "\t"; // << searching[index] << std::endl;
        }
    }

    std::cout << "End of program" << std::endl;

    return 0;
}
