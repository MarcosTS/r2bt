
// Include data structures and algorithms
#include <r2bt/r2bt.hpp>

// Include utils headers
#include <r2bt/utils/example_point.hpp>
#include <r2bt/utils/xmlReader.hpp>

// using namespace instead of type r2bt:: everywhere
using namespace r2bt;

// Replace with your own absolute or relative path
#define DATA_FOLDER "../../test-data/"

int main() {

    // Data filename (using provided data at test-data folder
    const char* sp_filename = DATA_FOLDER "gear-fem.vtu";
    const char* qp_filename = DATA_FOLDER "gear-spots.vtu";

    // Vectors to store points
    std::vector< example_point<3> > searching_points;
    std::vector< example_point<3> > querying_points;

    // Loading points from data files
    load_file(sp_filename, searching_points);
    load_file(qp_filename, querying_points);

    const double h = 0.2; // cell size suggestion, will be rounded up to a power of 2
    const double radius = 0.1; // radius should be less than h. About half the h

    // Creating searching set (kept as an smart pointer):
    auto s_set = factory::createUniquePtr< 3, example_point<3> >("Serial", h, "stree");

    // Populating searching set:
    insert( s_set.get(), searching_points.begin(), searching_points.end(), example2coordinates<3> );

    // Querying:
    auto result = query(s_set.get(), querying_points.begin(), querying_points.end(), querying_points.size(), example2coordinates<3>, radius);

    // Show results. Output results are an associate container, its keys are the indexes of the querying points
    // and the values are a container of indexes of searching points. Both indexes with respect of the original vectors.

    // First 5 querying points:
    for ( size_t i = 0; i < 5; ++i ) {
        auto neighbors = result[i]; // Select neighbors of querying point index i
        std::cout << "++++ Neighbors of: " << querying_points[i] << "++++" << std::endl;
        std::cout << std::endl;
        for ( auto ite = neighbors.begin(); ite != neighbors.end(); ++ite ) {
            // Show neighbor index and point
            std::cout << "Index:" << *ite << " Point: " << searching_points[*ite] << std::endl;
        }
        std::cout << std::endl;
    }

    // Last 5 querying points:
    size_t len = querying_points.size();
    for ( size_t i = len-5; i < len; ++i ) {
        auto neighbors = result[i];
        std::cout << "++++ Neighbors of: " << querying_points[i] << "++++" << std::endl;
        std::cout << std::endl;
        for ( auto ite = neighbors.begin(); ite != neighbors.end(); ++ite ) {
            std::cout << "Index:" << *ite << " Point: " << searching_points[*ite] << std::endl;
        }
        std::cout << std::endl;
    }

    return 0;
}
